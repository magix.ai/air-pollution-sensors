from kerastuner import HyperParameters
from tensorflow import keras
from tensorflow.keras import layers
from lstm_models import ModelParams
from model_1 import Model1


class Model3(Model1):

    def __init__(self, model_params: ModelParams, model_path_prefix='Case_3_', load_model=True):
        super().__init__(model_params=model_params, model_path_prefix=model_path_prefix, load_model=load_model)

    def build(self, hp: HyperParameters):
        n_features = self.model_params.n_features
        model_input_shape = (self.model_params.n_steps, n_features)

        model = keras.Sequential()
        units = hp.Int('initial_unit', min_value=2, max_value=24, step=2)
        model.add(layers.LSTM(units=units, activation=self.model_params.activation, input_shape=model_input_shape))
        model.add(layers.Dense(1))

        model.compile(
            optimizer=keras.optimizers.Adam(
                hp.Choice('learning_rate', [0.01, 0.1])),
            loss=self.model_params.loss,
            metrics=self.model_params.metrics)
        return model
