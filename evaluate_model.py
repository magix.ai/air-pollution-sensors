import lstm_models as ap_models
import data_set as ap_data_set
from data_set import DataSetDefinition
from data_set import DataSetFactory
from hyper_models import AirQualityHyperModel
from model_lstm import ModelLstm
from model_lstm_with_dropout import ModelLstmWithDropout
import time

if __name__ == "__main__":

    in_sequences = [
        ap_data_set.KARPOS_PM10,
        ap_data_set.KARPOS_O3,
        ap_data_set.REKTORAT_PM10,
        ap_data_set.REKTORAT_O3,
        ap_data_set.MILADINOVCI_PM10,
        ap_data_set.MILADINOVCI_O3,
        ap_data_set.CENTAR_PM10,
        ap_data_set.CENTAR_O3,
        ap_data_set.T_C,
        ap_data_set.P0_HPA
    ]

    data_set_definition = DataSetDefinition(in_sequences, out_seq=ap_data_set.KARPOS_PM10_3, num_steps=24)
    data_set = DataSetFactory.create('2011-12-01', '2020-03-01', data_set_definition)
    #tune_training_data = DataSetFactory.create('2014-08-01', '2016-08-01', data_set_definition)
    #tune_validation_data = DataSetFactory.create('2016-11-01', '2016-12-31', data_set_definition)

    data_set.plot(ap_data_set.KARPOS_PM10)
    data_set.plot(ap_data_set.KARPOS_O3)
    data_set.plot(ap_data_set.REKTORAT_PM10)
    data_set.plot(ap_data_set.REKTORAT_O3)
    data_set.plot(ap_data_set.MILADINOVCI_PM10)
    data_set.plot(ap_data_set.MILADINOVCI_O3)
    data_set.plot(ap_data_set.CENTAR_PM10)
    data_set.plot(ap_data_set.CENTAR_O3)
    data_set.plot(ap_data_set.T_C)
    data_set.plot(ap_data_set.P0_HPA)
    # data_set.plot(ap_data_set.P_TND)
    exit(0)

    model_params = ap_models.ModelParams(data_set, metrics=[])
    model_params.epochs = 10
    model_params.activation = 'relu'
    model_params.tune_epochs = 2

    tic = time.perf_counter()
    aq_model = ModelLstmWithDropout(model_params=model_params, load_model=True)
    aq_model.model_path_prefix = 'Case_6_'
    # aq_model = ModelLstm(model_params=model_params, load_model=False)
    aq_model.tune(tune_training_data, tune_validation_data)
    aq_model.train(data_set)
    toc = time.perf_counter()

    #aq_model.plot_prediction(data_set, sample_index=0)
    aq_model.summary()
    # aq_model.save_model()
    aq_model.plot_metrics()
    exec_time = toc-tic
    print(exec_time)
