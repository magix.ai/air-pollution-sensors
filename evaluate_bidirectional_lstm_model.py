import lstm_models as ap_models
from data_set import DataSetFactory
from data_set import DataSetDefinitionFactory
from model_bidirectional_lstm import ModelBidirectionalLstm
import time
from model_evaluate import ModelEvaluate

if __name__ == "__main__":

    model = ModelEvaluate()
    aq_model = ModelBidirectionalLstm(model_params=model.model_params, model_path_prefix='Case_8_', load_model=True)
    model.evaluate(aq_model, start_date='2020-01-01', end_date='2020-02-01')
    model.compare_to_arima(aq_model, start_date='2020-01-01', end_date='2020-01-04')
