from kerastuner import HyperParameters
from tensorflow import keras
from tensorflow.keras.layers import LSTM
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import SimpleRNN
from hyper_models import AirPollutionForecastingModel
from tensorflow.keras.layers import Input


class ForecastingModel7(AirPollutionForecastingModel):

    def build(self, hp: HyperParameters):
        model = keras.Sequential()
        units = hp.Int('initial_unit', min_value=2, max_value=24, step=2)
        rnn_units = hp.Int('rnn_unit', min_value=1, max_value=128, step=4)

        model_input_shape = (self.model_params.n_steps, self.model_params.n_features)

        model.add(Input(shape=model_input_shape))
        print(model.output)
        # The output of SimpleRNN will be a 2D tensor of shape (batch_size, rnn_units)
        model.add(SimpleRNN(rnn_units, activation=self.model_params.activation))
        print(model.output)
        model.add(Dense(1))
        print(model.output)

        model.compile(
            optimizer=keras.optimizers.Adam(hp.Choice('learning_rate', [0.01, 0.1])),
            loss=self.model_params.loss,
            metrics=self.model_params.metrics)
        return model
