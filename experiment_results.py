import matplotlib.pyplot as plt
from sklearn.metrics import mean_squared_error
import os
from pandas import Series
from pandas import read_csv
from pandas import DatetimeIndex
import data_set as data_set
from experiment import ExperimentModel


class Results:
    def __init__(self):
        self.predictions = list()
        self.real_values = list()
        self.y = Series(self.real_values)
        self.y_hat = Series(self.predictions)
        self.error = None
        self.model_class = None
        self.results_plot = ResultsPlot()
        self.start = None
        self.end = None
        self.experiment_model = None
        self.mse = None
        self.rmse = None
        self.plot_label = None

    @staticmethod
    def get_yhat_file_for_experiment(model_class: str, experiment_model: ExperimentModel) -> str:
        return Results.get_yhat_file_name(
            model_class,
            experiment_model.forecast,
            experiment_model.ns_out_seq,
            experiment_model.test_data_interval.start_date,
            experiment_model.test_data_interval.end_date,
            len(experiment_model.in_seq)
        )

    @staticmethod
    def get_yhat_file_name(model_class: str, forecast_step: int, ns_out_seq: str, start: str, end: str,
                           in_seq_num: int):
        file_path = 'yhat__' + ns_out_seq + '__' + start + '_' + end + '__' + str(in_seq_num) + '.csv'
        model_results_path = Results.build_model_results_path(model_class, forecast_step)
        return os.path.join(model_results_path, file_path)

    @staticmethod
    def get_y_file_name(model_class: str, forecast_step: int, ns_out_seq: str, start: str, end: str, in_seq_num: int):
        file_path = 'y__' + ns_out_seq + '__' + start + '_' + end + '.csv'
        model_results_path = Results.build_model_results_path(model_class, forecast_step)
        return os.path.join(model_results_path, file_path)

    @staticmethod
    def build_model_results_path(model_class: str, forecast_step: int):
        base_path = os.path.dirname(os.path.abspath(__file__))
        base_path = os.path.join(base_path, 'evaluation')
        model_results_path = os.path.join(base_path, model_class)
        forecast_path = os.path.join(model_results_path, str(forecast_step))
        if not os.path.isdir(model_results_path):
            os.mkdir(model_results_path)
        if not os.path.isdir(forecast_path):
            os.mkdir(forecast_path)

        return forecast_path

    def metrics(self):
        forecasted_values = self.predictions
        measured_values = self.real_values

        if len(forecasted_values) == 0:
            self.trim()
            forecasted_values = self.y_hat
            measured_values = self.y

        self.error = mean_squared_error(measured_values, forecasted_values)
        self.mse = mean_squared_error(measured_values, forecasted_values)
        self.rmse = mean_squared_error(measured_values, forecasted_values, squared=False)

        print('Results metrics:')
        print('mse=%f, rmse=%f' % (self.mse, self.rmse))

    def trim(self, start_ts=None, end_ts=None):
        measurement_timestamps_min = self.y_hat.index.min()
        measurement_timestamps_max = self.y_hat.index.max()
        if start_ts is not None and start_ts > measurement_timestamps_min:
            measurement_timestamps_min = start_ts
        if end_ts is not None and end_ts < measurement_timestamps_max:
            measurement_timestamps_max = end_ts

        self.y = self.y[self.y.index >= measurement_timestamps_min]
        self.y = self.y[self.y.index <= measurement_timestamps_max]
        self.y_hat = self.y_hat[self.y_hat.index >= measurement_timestamps_min]
        self.y_hat = self.y_hat[self.y_hat.index <= measurement_timestamps_max]

    def reset(self):
        self.predictions = list()
        self.real_values = list()
        self.y = Series(self.real_values)
        self.y_hat = Series(self.predictions)

    def save(self, model_class: str, experiment_model: ExperimentModel):
        observations_file_path = Results.get_y_file_name(
            model_class,
            experiment_model.forecast,
            experiment_model.ns_out_seq,
            experiment_model.test_data_interval.start_date,
            experiment_model.test_data_interval.end_date,
            len(experiment_model.in_seq)
        )
        predictions_file_path = Results.get_yhat_file_name(
            model_class,
            experiment_model.forecast,
            experiment_model.ns_out_seq,
            experiment_model.test_data_interval.start_date,
            experiment_model.test_data_interval.end_date,
            len(experiment_model.in_seq)
        )
        self.experiment_model = experiment_model
        self.to_file(predictions_file_path, observations_file_path, self.experiment_model)

    def to_file(self, predictions_file_path: str, observations_file_path: str, experiment_model: ExperimentModel):
        self.y.to_csv(observations_file_path, header=[experiment_model.out_seq])
        self.y_hat.to_csv(predictions_file_path, header=[experiment_model.out_seq])

    def plot(self, predictions_label: str, observations_label: str):
        self.results_plot.plot(
            {
                observations_label: self.y,
                predictions_label: self.y_hat
            }
        )

    def compare(self, results: list):
        series = {
            ResultsPlot.PM10_VALUES: self.y,
            self.plot_label: self.y_hat
        }
        for result in results:
            series[result.plot_label] = result.y_hat

        self.results_plot.plot(series=series)


class ResultsFactory:

    @staticmethod
    def load_model_results(model_class: str, experiment_model: ExperimentModel, plot_label=None) -> Results:
        y_hat_file = Results.get_yhat_file_name(
            model_class,
            experiment_model.forecast,
            experiment_model.ns_out_seq,
            experiment_model.test_data_interval.start_date,
            experiment_model.test_data_interval.end_date,
            len(experiment_model.in_seq)
        )
        y_file = Results.get_y_file_name(
            model_class,
            experiment_model.forecast,
            experiment_model.ns_out_seq,
            experiment_model.test_data_interval.start_date,
            experiment_model.test_data_interval.end_date,
            len(experiment_model.in_seq)
        )

        if not os.path.isfile(y_hat_file) or not os.path.isfile(y_file):
            raise Exception('Results files do not exist: ' + y_hat_file)

        results = Results()
        results.experiment_model = experiment_model
        results.plot_label = model_class

        if plot_label is not None:
            results.plot_label = plot_label

        y_hat_df = read_csv(y_hat_file, index_col=0)
        results.y_hat = y_hat_df[experiment_model.out_seq]
        results.y_hat.index = DatetimeIndex(y_hat_df.index.to_list())

        y_df = read_csv(y_file, index_col=0)
        results.y = y_df[experiment_model.out_seq]
        results.y.index = DatetimeIndex(y_df.index.to_list())
        results.trim()

        return results


class ResultsPlot:
    ARIMA_FORECAST = 'ARIMA Forecast'
    FORECAST = 'Forecast'
    PM10_VALUES = 'PM10 Values'
    KARPOS_PM10 = data_set.KARPOS_PM10

    def __init__(self):
        self.series = {}
        self.colors = {
            ResultsPlot.PM10_VALUES: 'blue',
            ResultsPlot.ARIMA_FORECAST: 'red',
            ResultsPlot.FORECAST: 'springgreen',
            data_set.REKTORAT_PM10: 'blue',
            data_set.KARPOS_PM10: 'darkgreen',
            data_set.CENTAR_PM10: 'darkblue',
            data_set.MILADINOVCI_PM10: 'indigo'
        }

    def plot(self, series: dict):
        for k, v in series.items():
            plt.plot(v, color=self.colors[k], label=k)

        plt.legend(loc='upper left', fontsize=18)
        plt.show()
