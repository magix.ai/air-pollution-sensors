from kerastuner import HyperParameters
from tensorflow import keras
from tensorflow.keras.layers import TimeDistributed
from tensorflow.keras.layers import Conv1D
from tensorflow.keras.layers import LSTM
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import Flatten
from tensorflow.keras.layers import MaxPooling1D
from tensorflow.keras.layers import GRU
from tensorflow.keras.layers import SimpleRNN
from hyper_models import AirQualityHyperModel

# bad performance
class ModelRnn(AirQualityHyperModel):

    def build(self, hp: HyperParameters):
        n_features = self.model_params.n_features
        model_input_shape = (self.model_params.n_steps, n_features)

        model = keras.Sequential()
        units = hp.Int('initial_unit', min_value=2, max_value=24, step=2)

        # input_shape = (None, n_steps, n_features)
        # The output of GRU will be a 3D tensor of shape (batch_size, timesteps, 256)
        model.add(GRU(256, return_sequences=True))

        # The output of SimpleRNN will be a 2D tensor of shape (batch_size, 128)
        model.add(SimpleRNN(128, return_sequences=True))

        model.add(LSTM(units=units, activation=self.model_params.activation))
        model.add(Dense(1))

        model.compile(
            optimizer=keras.optimizers.Adam(hp.Choice('learning_rate', [0.01, 0.1])),
            loss=self.model_params.loss,
            metrics=self.model_params.metrics)
        return model
