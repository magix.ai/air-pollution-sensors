import numpy as np
from numpy import genfromtxt
import glob
import matplotlib.pylab as plt

def labelPollutionIndexPm10(pm10Value):
    return pm10Value // 100

def scalePollutionIndicesInDataTableAndReturnMeanLabel(dataTable):
    dataTable = dataTable[::-1]
    pm10Sum = 0
    numOfReadings = len(dataTable)
    for i in range(0, numOfReadings):
        pm10Sum += dataTable[i, 3]
        dataTable[i, 3] = labelPollutionIndexPm10(dataTable[i, 3])
    return labelPollutionIndexPm10(pm10Sum / numOfReadings)

def loadData():

    # Add custom path
    # path = r'C:\Users\DomainUser\...'
    path = '../../resources/Separate'
    allFiles = glob.glob(path + "/*.csv")
    singleFilePrefix = '\deep_learning_'

    numOfFiles = len(allFiles)

    # this should be changed, training: 40, test: 30, validation: 30
    numOfTrainingSamples = 7000
    numOfTestSamples = numOfFiles - numOfTrainingSamples

    # Note, col 1 is missing (date), and col sk_petrovec_T_(C) is missing (data format is not float) - just for now
    columns = [1, 2, 3, 4, 5, 7, 8, 9, 10, 11, 12, 13, 14, 15] # Include all pm10 columns
    #columns = [4, 5, 7, 8, 9, 10, 11, 12, 13, 14, 15]  # Only include single pm10 column

    numOfRowsInASingleFile = 8

    trainingArray = np.empty(shape=(numOfTrainingSamples, numOfRowsInASingleFile, len(columns)))
    testArray = np.empty(shape=(numOfTestSamples, numOfRowsInASingleFile, len(columns)))
    labels_training = np.zeros(shape=(numOfTrainingSamples,))
    labels_test = np.zeros(shape=(numOfTestSamples,))

    labelTypes = {}

    for i in range(numOfFiles - 1, 0, -1):
        filename = path + singleFilePrefix + str(i) + '.csv'
        entry = genfromtxt(filename, delimiter=';', usecols=columns, skip_header=1, dtype=float)
        meanLabel = scalePollutionIndicesInDataTableAndReturnMeanLabel(entry)
        n = numOfFiles - i - 1
        if n < numOfTestSamples:
            testArray[n] = entry
            labels_test[n] = meanLabel
        else:
            trainingArray[n - numOfTrainingSamples] = entry
            labels_training[n - numOfTrainingSamples] = meanLabel
        labelTypes[meanLabel] = 1
        # alist.append(entry)

    # converted = np.array([xi for xi in alist])

    # plt.plot(labels_training)
    # plt.ylabel('Labels')
    # plt.show()

    return (trainingArray, labels_training), (testArray, labels_test), len(labelTypes)
