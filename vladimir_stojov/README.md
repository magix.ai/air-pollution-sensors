# Air Quality Prediction

Air Quality Prediction Repository

Current progress:

7000/7000 [==============================] - 1s 72us/step - loss: 3.5345 - acc: 0.7807 - val_loss: 2.2012 - val_acc: 0.8634
<br/>
Test loss: 2.201241579719346
<br/>
Test accuracy: 0.8634304207119741

PM 10 values distribution (Only for a single region)
<br/>
![Scheme](images/LabelsDistribution.png)

Test Results
<br/>
![Scheme](images/TestResults2.png)

