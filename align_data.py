import numpy as np
import pandas as pd
import glob
import os
import helpers as h
from time import time
from datetime import datetime, timedelta
import pytz, math



def standardize_prices(df: pd.DataFrame, file_out: str, selected_avail_groups, selected_instance_types,
                       os_value: str = 'Linux/UNIX'):
    if os.path.isfile(file_out):
        df_merged_prices = pd.read_csv(file_out, header=0, sep='\t', index_col=0)
    else:
        df = df[df['Operating System'] == os_value]
        avail_groups = set(df['Availability Group'].values)

        # clean timestamps
        # df['Timestamp'] = df['Timestamp'].str.replace('T', ' ').str[:-6]
        df['Timestamp'] = pd.to_datetime(df['Timestamp'])

        # keep data only for selected instances
        selected_instance_types_set = set(selected_instance_types)
        df = df[df['API Name'].isin(selected_instance_types_set)]
        # df_merged = pd.DataFrame(index=df['Timestamp'].unique())
        df_merged = None

        avail_groups.intersection_update(set(selected_avail_groups))
        for ag in sorted(avail_groups):
            region_code = ag[: -1]
            df_ag = df[df['Availability Group'] == ag]
            h.log_line('Processing AZ:', ag, df_ag.shape)
            # df = pd.DataFrame(index=sorted(set(df_ag['Timestamp'].values)))
            for i, inst_type in enumerate(selected_instance_types):
                df_inst = df_ag[df_ag['API Name'] == inst_type][['Timestamp', 'Price']]
                df_inst = df_inst.reset_index(drop=True).set_index('Timestamp')
                price_col = ag + '_' + inst_type + '_Price'
                df_inst.rename(columns={'Price': price_col}, inplace=True)
                # df_merged = df_merged.join(df_inst)
                if df_merged is None:
                    df_merged = df_inst
                else:
                    df_merged = df_merged.join(df_inst, how="outer", sort=True)
                h.log_line('Processing:', i, ag, inst_type, 'Not null:',
                           len(df_merged) - sum(df_merged[price_col].isna()))
        df_merged_prices = trim_df(df_merged)
        h.log_line('Finished for', file_out)
        df_merged_prices.to_csv(file_out, index=True, sep='\t')
    return df_merged_prices


def trim_df(df: pd.DataFrame, selected_columns=None) -> pd.DataFrame:
    """

    :param df:
    :return:
    """
    if selected_columns is None:
        selected_columns = df.columns
    df = df.sort_index()  # make sure it's sorted after the joins
    df[selected_columns] = df[selected_columns].fillna(method='ffill')

    # find the first (oldest) row where all prices are available
    ts_first_all_prices_available = max(df[selected_columns].notna().idxmax())
    # now we have prices for all instances
    df_trimmed = df[df.index >= ts_first_all_prices_available]

    # alternatively we can fill in the missing prices retroactively
    # df_merged_prices2 = df.fillna(method='bfill')
    return df_trimmed


def standardize_files(folder_in, d_on_demand: dict, source='marek', os_value='Linux/UNIX'):
    """

    :param folder_in:
    :param d_on_demand: Dict containing on-demand prices
    :return:
    """
    t0 = time()
    # https://docs.aws.amazon.com/emr/latest/ManagementGuide/emr-supported-instance-types.html
    # afterwards, availability zones which still missed some of the remaining instances types were also removed

    folder_out = os.path.join(folder_in, 'analysis')
    os.makedirs(folder_out, exist_ok=True)
    file_merged = os.path.join(folder_out, 'aws_spot_prices_selected_merged.csv')
    file_aligned_last_known = os.path.join(folder_out, 'aws_spot_prices_selected_aligned_last_known.csv')
    file_aligned_avg = os.path.join(folder_out, 'aws_spot_prices_selected_aligned_avg.csv')
    if not os.path.isfile(file_merged):
        # all_instance_types = set([x[1] for x in d_on_demand.keys()])
        # all_regions = set([x[0] for x in d_on_demand.keys()])
        columns_marek = ['SPOTPRICEHISTORY', 'Availability Group', 'API Name', 'Operating System', 'Price', 'Timestamp']
        columns_kaggle = ['Timestamp', 'API Name', 'Operating System', 'Availability Group', 'Price']
        columns = columns_marek if source == 'marek' else columns_kaggle
        sep = '\t' if source == 'marek' else ','
        usecols = columns_marek[1:] if source == 'marek' else columns_kaggle
        files = glob.glob(os.path.join(folder_in, '*.csv'))
        df_az_selected = None
        for f in files:
            t1 = time()
            h.log_line('Starting processing file:', f)
            prefix, _ = os.path.splitext(f)
            os.makedirs(prefix, exist_ok=True)
            cur_file_out = os.path.join(prefix, os.path.basename(prefix) + '_merged_selected.csv')
            df = pd.read_csv(f, sep=sep, names=columns, usecols=usecols, header=None)
            # df['Operating System Type'] = np.where(df['Operating System'].str.contains('Linux'), 'Linux', 'Windows')
            df_az = standardize_prices(df, cur_file_out, selected_zones, inst_emr_ebs, os_value)
            if df_az_selected is None:
                df_az_selected = df_az
            else:
                df_az_selected = df_az_selected.join(df_az, how="outer", sort=True)
            h.log_line('Finished processing file:', f, 'Duration:', time() - t1, 'Current rows:', len(df_az),
                       'Total rows:', len(df_az_selected))
        df_az_selected = trim_df(df_az_selected)
        h.log_line('Starting to save into file:', file_merged, 'Expected size:', df_az_selected.shape)
        t2 = time()
        df_az_selected.to_csv(file_merged, index=True, sep='\t')
        h.log_line('Finished saving to file:', file_merged, 'Duration', time() - t2, 'Size:', df_az_selected.shape)
    else:
        t2 = time()
        df_az_selected = pd.read_csv(file_merged, index_col=0, date_parser=pd.to_datetime, parse_dates=[0], sep='\t')
        h.log_line('Finished loading from file:', file_merged, 'Duration', time() - t2, 'Size:', df_az_selected.shape)

    h.log_line('Total indexes', len(df_az_selected), 'First timestamp:', df_az_selected.index.values[0],
               'Last timestamp:', df_az_selected.index.values[-1])
    align_data_last_known(df_az_selected, file_aligned_last_known)
    align_data_avg(df_az_selected, file_aligned_avg)
    h.log_line('Finished processing all:', 'Duration:', time() - t0)


def dt64_to_datetime(dt64):
    ts = (dt64 - np.datetime64('1970-01-01T00:00:00Z')) / np.timedelta64(1, 's')
    d = datetime.utcfromtimestamp(ts)
    return d


def align_data_last_known(df: pd.DataFrame, filename_out, delta_seconds=120, decimals=5):
    t0 = time()
    # l = pd.to_datetime(df.index).values  # string to datetime
    # df_inst = df_inst.reset_index(drop=True).set_index('Timestamp')
    if not os.path.isfile(filename_out):
        first_ts = dt64_to_datetime(df.index.values[0])
        last_ts = dt64_to_datetime(df.index.values[-1])
        first_ts_valid = datetime(first_ts.year, first_ts.month, first_ts.day, first_ts.hour, first_ts.minute,
                                  0, tzinfo=pytz.UTC) + timedelta(minutes=1)
        last_ts_valid = datetime(last_ts.year, last_ts.month, last_ts.day, last_ts.hour, last_ts.minute, 0,
                                 tzinfo=pytz.UTC)

        time_span = last_ts_valid - first_ts_valid  # Calculate the time period
        num_increments = math.ceil(time_span.total_seconds() // delta_seconds)  # Calculate the number of increments
        increments_range = list(range(num_increments + 1))

        # create the even timestamps
        even_timestamps = [first_ts_valid + timedelta(seconds=i * delta_seconds) for i in increments_range]
        df_ts = pd.DataFrame(index=even_timestamps, data=increments_range, columns=['Timestamp Index'])
        df_ts.index.name = 'Timestamp Even'

        df_full = df_ts.join(df, how='outer')  # join with the real timestamps
        df_full = trim_df(df_full,
                          selected_columns=df.columns)  # fill in the missing values based on the last known values
        df_full.index.name = 'Timestamp'
        df_full = df_full[df_full['Timestamp Index'].notna()]
        df_full.drop(columns=['Timestamp Index'], inplace=True)
        df_full.to_csv(filename_out, sep='\t', index=True, float_format=f'%.{decimals}f')
        h.log_line('Finished aligning data. Duration:', time() - t0, 'Shape:', df_full.shape, 'Saved to:', filename_out)


def align_data_avg(df: pd.DataFrame, filename_out, delta_seconds=120, decimals=5):
    t0 = time()
    if not os.path.isfile(filename_out):
        first_ts = dt64_to_datetime(df.index.values[0])
        last_ts = dt64_to_datetime(df.index.values[-1])
        # even timestamps, e.g. 2019-12-12 17:58:00 (and not 2019-12-12 17:57:24.123)
        first_ts_even = datetime(first_ts.year, first_ts.month, first_ts.day, first_ts.hour, first_ts.minute,
                                 0, tzinfo=pytz.UTC) + timedelta(minutes=1)
        last_ts_even = datetime(last_ts.year, last_ts.month, last_ts.day, last_ts.hour, last_ts.minute, 0,
                                tzinfo=pytz.UTC)
        # print(first_ts, first_ts_even, last_ts, last_ts_even)

        time_span = last_ts_even - first_ts_even  # Calculate the time period
        num_increments = math.ceil(time_span.total_seconds() // delta_seconds)  # Calculate the number of increments
        increments_range = list(range(num_increments + 1))

        # create the even timestamps, e.g. 2019-12-12 17:58:00 (and not 2019-12-12 17:57:24.123)
        even_timestamps = [first_ts_even + timedelta(seconds=i * delta_seconds) for i in increments_range]
        df_ts = pd.DataFrame(index=even_timestamps, data=increments_range, columns=['Timestamp Index'])
        df_ts.index.name = 'Timestamp Even'

        df_full = df_ts.join(df, how='outer')  # join with the real timestamps

        # fill in the missing values for newly added rows with even timestamps based on the last known values
        df_full = trim_df(df_full)
        df_full.index.name = 'Timestamp'

        new_index = df_full.index.values[:-1]
        # now calculate the differences of timestamps so we can get the deltas between consecutive records
        deltas = df_full.index.values[1:] - new_index  # deltas between consecutive timestamps

        # convert detlas to seconds and convert it as a ratio from the hour
        duration_ratio = np.array(pd.to_timedelta(deltas).seconds) / 3600

        price_columns = list(df.columns)

        # calculate the total cost during a specific time segment between consecutive rows
        # compatible for matrix multiplication
        data_price = df_full[price_columns].values[:-1, :] * duration_ratio.reshape(-1, 1)
        data = np.concatenate((df_full['Timestamp Index'].values[:-1].reshape(-1, 1), data_price), axis=1)
        df_prices = pd.DataFrame(index=new_index, columns=['Timestamp Index'] + price_columns, data=data)
        df_prices.index.name = 'Timestamp'
        hour_multiplier = 3600 / delta_seconds
        df_prices_mean = df_prices.groupby(by='Timestamp Index').sum() * hour_multiplier
        df_prices_mean.index = even_timestamps
        df_prices_mean.index.name = 'Timestamp'
        df_prices_mean.to_csv(filename_out, sep='\t', index=True, float_format=f'%.{decimals}f')
        h.log_line('Finished aligning data. Duration:', time() - t0, 'Shape:', df_prices_mean.shape, 'Saved to:',
                   filename_out)


def verify_alignment(file_aligned_last_known, file_aligned_avg, file_diff):
    lk = pd.read_csv(file_aligned_last_known, index_col=0, date_parser=pd.to_datetime, parse_dates=[0], sep='\t')
    avg = pd.read_csv(file_aligned_avg, index_col=0, date_parser=pd.to_datetime, parse_dates=[0], sep='\t')
    diff = pd.DataFrame(index=lk.index, data=lk.values - avg.values, columns=lk.columns)
    # diff2 = diff[(diff.T != 0).any()]
    diff2 = diff.loc[~(diff == 0).all(axis=1)]
    diff2.to_csv(file_diff, sep='\t', index=True, float_format=f'%.5f')


df_on_demand = pd.read_csv('/Users/eftim/Datasets/AWS_spot_instances/aws_on_demand_prices.csv',
                           sep='\t', index_col=['Region Code', 'API Name'], header=0)
d_on_demand = df_on_demand.to_dict(orient='index')
# print(d_on_demand[('eu-west-1', 'm5dn.xlarge')])

# analyze_frequency_all('/Users/eftim/Datasets/AWS_spot_instances/AWS-spot-2020-01-11')
# standardize_files('/Users/eftim/Datasets/AWS_spot_instances/AWS-spot-2020-01-11', d_on_demand)
verify_alignment(
    '/Users/eftim/Datasets/AWS_spot_instances/AWS-spot-2020-01-11/analysis/aws_spot_prices_selected_aligned_last_known.csv',
    '/Users/eftim/Datasets/AWS_spot_instances/AWS-spot-2020-01-11/analysis/aws_spot_prices_selected_aligned_avg.csv',
    '/Users/eftim/Datasets/AWS_spot_instances/AWS-spot-2020-01-11/analysis/aws_spot_prices_selected_aligned_diff.csv')
