from model_cnn_lstm import ModelCnnLstm
from model_evaluate import ModelEvaluate

if __name__ == "__main__":

    model = ModelEvaluate()
    aq_model = ModelCnnLstm(model_params=model.model_params, model_path_prefix = 'Case_7_', load_model=True)
    model.evaluate(aq_model, start_date='2020-01-01', end_date='2020-02-01')
    model.compare_to_arima(aq_model, start_date='2020-01-01', end_date='2020-01-04')
