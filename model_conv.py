from kerastuner import HyperParameters
from tensorflow import keras
from tensorflow.keras.layers import TimeDistributed
from tensorflow.keras.layers import Conv1D
from tensorflow.keras.layers import LSTM
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import ConvLSTM2D
from tensorflow.keras.layers import Flatten
from tensorflow.keras.layers import MaxPooling1D
from tensorflow.keras.layers import GRU
from tensorflow.keras.layers import SimpleRNN
from hyper_models import AirQualityHyperModel
from tensorflow.keras.layers import Bidirectional
from tensorflow.keras.layers import BatchNormalization


class ModelConv(AirQualityHyperModel):

    def build(self, hp: HyperParameters):
        n_features = self.model_params.n_features
        n_steps = self.model_params.n_steps
        model_input_shape = (self.model_params.n_steps, n_features)

        model = keras.Sequential()
        units = hp.Int('initial_unit', min_value=2, max_value=24, step=2)
        second_lstm_layer_units = hp.Int('second_lstm_layer_unit', min_value=2, max_value=124, step=4)

        # The output of SimpleRNN will be a 2D tensor of shape (batch_size, 128)
        # model.add(SimpleRNN(128, return_sequences=True))
        # model.add(Bidirectional(LSTM(units=units, activation=self.model_params.activation),
        #                         input_shape=model_input_shape))

        model.add(ConvLSTM2D(filters=40, kernel_size=(n_features, n_steps),
                       input_shape=(None, None, n_features, n_steps),
                       padding='same'))
        model.add(TimeDistributed(Flatten()))
        #model.add(BatchNormalization())
        #model.add(LSTM(units=units, activation=self.model_params.activation))
        model.add(TimeDistributed(MaxPooling1D(pool_size=2)))
        model.add(Dense(1))

        model.compile(
            optimizer=keras.optimizers.Adam(hp.Choice('learning_rate', [0.01, 0.1])),
            loss=self.model_params.loss,
            metrics=self.model_params.metrics)
        return model
