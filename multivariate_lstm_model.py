from numpy import array
from numpy import hstack
from keras.models import Sequential
from keras.layers import LSTM
from keras.layers import Dense
import pandas as pd
import configparser
import preprocessing
import os
import numpy as np


# split a multivariate sequence into samples
def split_sequences(sequences, n_steps):
    X, y = list(), list()
    for i in range(len(sequences)):
        # find the end of this pattern
        end_ix = i + n_steps
        # check if we are beyond the dataset
        if end_ix > len(sequences):
            break
        # gather input and output parts of the pattern
        seq_x, seq_y = sequences[i:end_ix, :-1], sequences[end_ix - 1, -1]
        X.append(seq_x)
        y.append(seq_y)
    return array(X), array(y)


def train_model(in_sequences: list, out_seq: np.array, num_steps: int):
    # convert to [rows, columns] structure
    dataset = prepare_dataset(in_sequences, out_seq)

    # choose a number of time steps
    n_steps = num_steps
    # convert into input/output
    X, y = split_sequences(dataset, n_steps)
    # the dataset knows the number of features, e.g. 2
    n_features = X.shape[2]
    # define model
    model = Sequential()
    model.add(LSTM(50, activation='relu', input_shape=(n_steps, n_features)))
    model.add(Dense(1))
    model.compile(optimizer='adam', loss='mse')
    # fit model
    model.fit(X, y, epochs=100, verbose=0)
    return model


def prepare_dataset(in_sequences: list, out_seq):
    init_seq = in_sequences.pop()
    data_set = init_seq.reshape((len(init_seq), 1))

    for in_seq in in_sequences:
        in_seq = in_seq.reshape((len(in_seq), 1))
        data_set = hstack((data_set, in_seq))

    out_seq = out_seq.reshape((len(out_seq), 1))
    return hstack((data_set, out_seq))


def predict(model, in_sequences: list, out_seq: np.array, num_steps: int):
    # demonstrate prediction
    # convert to [rows, columns] structure
    dataset = prepare_dataset(in_sequences, out_seq)

    # choose a number of time steps
    n_steps = num_steps
    # convert into input/output
    X, y = split_sequences(dataset, n_steps)
    n_features = X.shape[2]

    for i in range(len(X)):
        x_input = X[i]
        x_input = x_input.reshape((1, n_steps, n_features))
        yhat = model.predict(x_input, verbose=0)
        print(x_input, yhat)
