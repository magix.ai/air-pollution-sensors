from keras import Sequential
from keras.layers import LSTM
from keras.layers import Dense
from keras.layers import Bidirectional
from keras.layers import TimeDistributed
from keras.layers.convolutional import Conv1D
from keras.layers.convolutional import MaxPooling1D
from keras.layers import Flatten
from keras.callbacks.callbacks import History
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from data_set import TimeSeriesDataSet
from tensorflow.keras import activations


class ModelParams:
    def __init__(self, data_set: TimeSeriesDataSet, metrics: list):
        self.epochs = 10
        self.verbose = 2
        self.tune_epochs = 2
        self.max_trials = 10

        self.n_steps = data_set.num_steps
        self.activation = activations.relu
        self.data_set = data_set
        self.X, self.y = data_set.as_model_data()
        self.n_features = self.X.shape[2]
        self.metrics = metrics
        self.loss = 'mse'
        self.validation_split = 0.01


class SigmoidModelParams:

    def __init__(self, data_set: TimeSeriesDataSet, metrics: list):
        self.epochs = 10
        self.verbose = 2
        self.tune_epochs = 2

        self.n_steps = data_set.num_steps
        self.activation = activations.sigmoid # 'relu'
        self.data_set = data_set
        self.X, self.y = data_set.as_model_data()
        self.n_features = self.X.shape[2]
        self.metrics = metrics
        self.loss = 'mse'
        self.validation_split = 0.01



class LstmModel:
    def __init__(self, lstm_model: Sequential, model_params: ModelParams):
        self.model = lstm_model
        self.model_params = model_params
        self.history = History()

    def compile(self):
        self.model.compile(
            optimizer='adam',
            loss='mse',
            metrics=self.model_params.metrics
        )

    def train(self):
        self.compile()
        # fit model
        self.history = self.model.fit(
            x=self.model_params.X,
            y=self.model_params.y,
            epochs=self.model_params.epochs,
            verbose=self.model_params.verbose,
            validation_split=0.01
        )

    def plot_metrics(self):
        # Plot training & validation accuracy values
        plt.plot(self.history.history['acc'])
        plt.plot(self.history.history['val_acc'])
        plt.title('Model accuracy')
        plt.ylabel('Accuracy')
        plt.xlabel('Epoch')
        plt.legend(['Train', 'Test'], loc='upper left')
        plt.show()

    def predict(self, x_input):
        x_input = x_input.reshape((1, self.model_params.n_steps, self.model_params.n_features))
        yhat = self.model.predict(x_input, verbose=0)
        return yhat

    def predict_set(self, X_input: np.array) -> pd.Series:
        s = pd.Series()
        for i in range(len(X_input)):
            x_in = X_input[i]
            yhat = self.predict(x_in)
            s.append(yhat)
        return s

    def verify(self, data_set: TimeSeriesDataSet, print_predictions: bool = True) -> pd.Series:
        # convert into input/output
        X, y = data_set.as_model_data()

        predictions = pd.Series()
        error_series = pd.Series()

        for i in range(len(X)):
            x_input = X[i]
            yhat = self.predict(x_input)
            predictions.append(yhat)

            if print_predictions:
                print(x_input, yhat, y[i])

            error_series.append(yhat - y[i])

        return error_series


class LstmModelFactory:

    MODEL_VANILLA_LSTM = 'VANILLA_LSTM'
    MODEL_STACKED_LSTM = 'STACKED_LSTM'
    MODEL_BIDIRECTIONAL_LSTM = 'BIDIRECTIONAL_LSTM'
    MODEL_CNN_LSTM = 'CNN_LSTM'

    @staticmethod
    def create(lstm_model: str, model_params: ModelParams) -> LstmModel:
        models = {
            LstmModelFactory.MODEL_VANILLA_LSTM: factory_vanilla_lstm_model,
            LstmModelFactory.MODEL_STACKED_LSTM: factory_stacked_lstm_model,
            LstmModelFactory.MODEL_BIDIRECTIONAL_LSTM: factory_bidirectional_lstm_model,
            LstmModelFactory.MODEL_CNN_LSTM: factory_cnn_lstm_model
        }

        return LstmModel(models[lstm_model](model_params), model_params)


def factory_cnn_lstm_model(model_params: ModelParams) -> Sequential:
    model = Sequential()
    # input_shape = (None, n_steps, n_features)
    model.add(TimeDistributed(Conv1D(filters=64, kernel_size=1, activation='relu'),
                              input_shape=(None, model_params.n_steps, model_params.n_features)))

    model.add(TimeDistributed(MaxPooling1D(pool_size=2)))
    model.add(TimeDistributed(Flatten()))
    model.add(LSTM(50, activation=model_params.activation))
    model.add(Dense(1))
    return model


def factory_vanilla_lstm_model(model_params: ModelParams) -> Sequential:
    # input_shape=(n_steps, n_features)
    model_input_shape = (model_params.n_steps, model_params.n_features)
    model = Sequential()
    model.add(LSTM(50, activation=model_params.activation, input_shape=model_input_shape))
    model.add(Dense(1))
    return model


def factory_stacked_lstm_model(model_params: ModelParams) -> Sequential:
    model = Sequential()
    # input_shape=(n_steps, n_features)
    model_input_shape = (model_params.n_steps, model_params.n_features)
    model.add(LSTM(50, activation=model_params.activation, return_sequences=True, input_shape=model_input_shape))
    model.add(LSTM(50, activation=model_params.activation))
    model.add(Dense(1))
    return model


def factory_bidirectional_lstm_model(model_params: ModelParams) -> Sequential:
    # input_shape=(n_steps, n_features)
    model_input_shape = (model_params.n_steps, model_params.n_features)
    model = Sequential()
    model.add(Bidirectional(LSTM(50, activation=model_params.activation), input_shape=model_input_shape))
    model.add(Dense(1))
    return model
