from kerastuner import HyperParameters
from tensorflow import keras
from tensorflow.keras.layers import TimeDistributed
from tensorflow.keras.layers import Conv1D
from tensorflow.keras.layers import LSTM
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import Flatten
from tensorflow.keras.layers import MaxPooling1D
from tensorflow.keras.layers import GRU
from tensorflow.keras.layers import SimpleRNN
from hyper_models import AirQualityHyperModel


class Model5(AirQualityHyperModel):

    def build(self, hp: HyperParameters):
        model = keras.Sequential()
        units = hp.Int('initial_unit', min_value=2, max_value=24, step=2)
        gru_units = hp.Int('gru_unit', min_value=12, max_value=256, step=4)
        rnn_units = hp.Int('rnn_unit', min_value=1, max_value=128, step=4)

        # input_shape = (None, n_steps, n_features)
        # The output of GRU will be a 3D tensor of shape (batch_size, timesteps, 256)
        model.add(GRU(gru_units, return_sequences=True, activation=self.model_params.activation))

        # The output of SimpleRNN will be a 2D tensor of shape (batch_size, 128)
        model.add(SimpleRNN(rnn_units, return_sequences=True, activation=self.model_params.activation))
        model.add(LSTM(units=units, activation=self.model_params.activation))
        model.add(Dense(1))

        model.compile(
            optimizer=keras.optimizers.Adam(hp.Choice('learning_rate', [0.01, 0.1])),
            loss=self.model_params.loss,
            metrics=self.model_params.metrics)
        return model
