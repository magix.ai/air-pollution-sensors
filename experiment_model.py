import data_set as ds
from tensorflow.keras import activations


class ExperimentModel:
    def __init__(self, out_seq: str, in_seq: list = None, forecast=6, activation=activations.relu):
        self.forecast = forecast
        self.out_seq = out_seq
        self.in_seq = in_seq
        if self.in_seq is None:
            self.in_seq = ds.DataSetDefinitionFactory.get_in_sequences()
        self.training_data_interval = ds.DataSetInterval(start_date='2011-12-01', end_date='2019-12-31')
        self.tune_training_data_interval = ds.DataSetInterval(start_date='2014-08-01', end_date='2016-08-01')
        self.tune_validation_data_interval = ds.DataSetInterval(start_date='2016-11-01', end_date='2016-12-31')
        self.test_data_interval = ds.DataSetInterval(start_date='2020-01-01', end_date='2020-02-01')
        self.window_length = 12  # Default data window length

        os_parts = out_seq.split('__')
        self.ns_out_seq = os_parts[0]
        self.activation = activation
        self.max_trials = 10


class ExperimentModelYear6(ExperimentModel):

    def __init__(self, out_seq: str, in_seq: list = None, activation=activations.relu):
        super().__init__(out_seq, in_seq, forecast=6, activation=activation)
        self.training_data_interval = ds.DataSetInterval(start_date='2011-12-01', end_date='2019-01-31')
        self.tune_training_data_interval = ds.DataSetInterval(start_date='2014-08-01', end_date='2016-08-01')
        self.tune_validation_data_interval = ds.DataSetInterval(start_date='2016-11-01', end_date='2016-12-31')
        self.test_data_interval = ds.DataSetInterval(start_date='2019-02-01', end_date='2020-02-01')


class Window24ExperimentModel12(ExperimentModel):

    def __init__(self, out_seq: str, in_seq: list = None):
        super().__init__(out_seq, in_seq, forecast=12)
        self.window_length = 24


class Window24ExperimentModel24(ExperimentModel):

    def __init__(self, out_seq: str, in_seq: list = None):
        super().__init__(out_seq, in_seq, forecast=24)
        self.window_length = 24


class ExperimentModel12(ExperimentModel):

    def __init__(self, out_seq: str, in_seq: list = None, activation=activations.relu):
        super().__init__(out_seq, in_seq, forecast=12, activation=activation)


class ExperimentModel24(ExperimentModel):

    def __init__(self, out_seq: str, in_seq: list = None, activation=activations.relu):
        super().__init__(out_seq, in_seq, forecast=24, activation=activation)


class ArimaExperimentModel(ExperimentModel):

    def __init__(self, out_seq: str, in_seq: list = None):
        super().__init__(out_seq, in_seq)
        self.training_data_interval = ds.DataSetInterval(start_date='2019-12-15', end_date='2019-12-31')
        self.test_data_interval = ds.DataSetInterval(start_date='2020-01-01', end_date='2020-02-01')


class ArimaExperimentModel6(ExperimentModel):

    def __init__(self, out_seq: str, in_seq: list = None):
        super().__init__(out_seq, in_seq)
        self.training_data_interval = ds.DataSetInterval(start_date='2019-11-01', end_date='2019-12-31')
        self.test_data_interval = ds.DataSetInterval(start_date='2020-01-01', end_date='2020-02-01')
        self.forecast = 6


class ArimaExperimentModel6Light(ExperimentModel):

    def __init__(self, out_seq: str, in_seq: list = None):
        super().__init__(out_seq, in_seq)
        self.training_data_interval = ds.DataSetInterval(start_date='2019-10-01', end_date='2019-12-31')
        self.test_data_interval = ds.DataSetInterval(start_date='2020-01-01', end_date='2020-02-01')
        self.forecast = 6


class ArimaExperimentModel6Year(ExperimentModel):

    def __init__(self, out_seq: str, in_seq: list = None):
        super().__init__(out_seq, in_seq)
        self.training_data_interval = ds.DataSetInterval(start_date='2019-01-01', end_date='2019-01-31')
        self.test_data_interval = ds.DataSetInterval(start_date='2019-02-01', end_date='2020-02-01')
        self.forecast = 6


class ArimaExperimentModel12(ArimaExperimentModel):

    def __init__(self, out_seq: str, in_seq: list = None):
        super().__init__(out_seq, in_seq)
        self.training_data_interval = ds.DataSetInterval(start_date='2019-12-15', end_date='2019-12-31')
        self.test_data_interval = ds.DataSetInterval(start_date='2020-01-01', end_date='2020-02-01')
        self.forecast = 12


class ArimaExperimentModel12Year(ArimaExperimentModel12):

    def __init__(self, out_seq: str, in_seq: list = None):
        super().__init__(out_seq, in_seq)
        self.training_data_interval = ds.DataSetInterval(start_date='2019-01-01', end_date='2019-01-31')
        self.test_data_interval = ds.DataSetInterval(start_date='2019-02-01', end_date='2020-02-01')


class ArimaExperimentModel24(ArimaExperimentModel):

    def __init__(self, out_seq: str, in_seq: list = None):
        super().__init__(out_seq, in_seq)
        self.training_data_interval = ds.DataSetInterval(start_date='2019-12-15', end_date='2019-12-31')
        self.test_data_interval = ds.DataSetInterval(start_date='2020-01-01', end_date='2020-02-01')
        self.forecast = 24


class ArimaExperimentModel24Year(ArimaExperimentModel24):

    def __init__(self, out_seq: str, in_seq: list = None):
        super().__init__(out_seq, in_seq)
        self.training_data_interval = ds.DataSetInterval(start_date='2019-01-01', end_date='2019-01-31')
        self.test_data_interval = ds.DataSetInterval(start_date='2019-02-01', end_date='2020-02-01')


class ExtendedDataSetModel(ExperimentModel):

    def __init__(self, out_seq: str, in_seq: list = None, activation=activations.relu):
        super().__init__(out_seq, in_seq, activation=activation)
        self.in_seq = ds.DataSetDefinitionFactory.get_extended_in_sequences()


class ExtendedDataSetModel12(ExtendedDataSetModel):

    def __init__(self, out_seq: str, in_seq: list = None, activation=activations.relu):
        super().__init__(out_seq, in_seq, activation=activation)
        self.forecast = 12


class ExtendedDataSetModel24(ExtendedDataSetModel):

    def __init__(self, out_seq: str, in_seq: list = None, activation=activations.relu):
        super().__init__(out_seq, in_seq, activation=activation)
        self.forecast = 24


class CompleteDataSetModel(ExperimentModel):

    def __init__(self, out_seq: str, in_seq: list = None):
        super().__init__(out_seq, in_seq)
        self.in_seq = ds.DataSetDefinitionFactory.get_all_in_sequences()
        self.training_data_interval = ds.DataSetInterval(start_date='2011-12-01', end_date='2020-03-08')


class CompleteDataSetModel12(CompleteDataSetModel):
    def __init__(self, out_seq: str, in_seq: list = None):
        super().__init__(out_seq, in_seq)
        self.forecast = 12


class No2DataSetModel12(ExperimentModel12):
    def __init__(self, out_seq: str, in_seq: list = None):
        super().__init__(out_seq, in_seq)
        self.in_seq = [
            ds.KARPOS_PM10,
            ds.REKTORAT_PM10,
            ds.MILADINOVCI_PM10,
            ds.CENTAR_PM10,
            ds.T_C,
            ds.P0_HPA,
            ds.KARPOS_NO2
        ]
        self.forecast = 12


class PM25DataSetModel12(ExperimentModel12):
    def __init__(self, out_seq: str, in_seq: list = None):
        super().__init__(out_seq, in_seq)
        self.in_seq = [
            ds.KARPOS_PM10,
            ds.REKTORAT_PM10,
            ds.MILADINOVCI_PM10,
            ds.CENTAR_PM10,
            ds.T_C,
            ds.P0_HPA,
            ds.KARPOS_PM25
        ]


class PM25DataSetModel24(ExperimentModel24):
    def __init__(self, out_seq: str, in_seq: list = None, activation=activations.relu):
        super().__init__(out_seq, in_seq, activation=activation)
        self.in_seq = [
            ds.KARPOS_PM10,
            ds.REKTORAT_PM10,
            ds.MILADINOVCI_PM10,
            ds.CENTAR_PM10,
            ds.T_C,
            ds.P0_HPA,
            ds.KARPOS_PM25
        ]


class PM25TmaxTminDataSetModel12(CompleteDataSetModel):
    def __init__(self, out_seq: str, in_seq: list = None):
        super().__init__(out_seq, in_seq)
        self.in_seq = [
            ds.KARPOS_PM10,
            ds.REKTORAT_PM10,
            ds.MILADINOVCI_PM10,
            ds.CENTAR_PM10,
            ds.T_C,
            ds.P0_HPA,
            ds.KARPOS_PM25,
            ds.T_MAX,
            ds.T_MIN
        ]
        self.forecast = 12


class PM25FullDataSetModel12(CompleteDataSetModel):
    def __init__(self, out_seq: str, in_seq: list = None):
        super().__init__(out_seq, in_seq)
        self.in_seq = [
            ds.KARPOS_PM10,
            ds.REKTORAT_PM10,
            ds.MILADINOVCI_PM10,
            ds.CENTAR_PM10,
            ds.T_C,
            ds.P0_HPA,
            ds.KARPOS_PM25,
            #ds.REKTORAT_PM25,
            ds.CENTAR_PM25,
            #ds.MILADINOVCI_PM25
        ]
        self.forecast = 12


class ExperimentModelFactory:

    @staticmethod
    def create_model(out_seq: str):
        #training_data_interval = ds.DataSetInterval(start_date='2011-12-01', end_date='2019-12-31')
        #tune_training_data_interval = ds.DataSetInterval(start_date='2014-08-01', end_date='2016-08-01')
        #tune_validation_data_interval = ds.DataSetInterval(start_date='2016-11-01', end_date='2016-12-31')
        return ExperimentModel(out_seq=out_seq)

    @staticmethod
    def create_fedcsis_article_model():
        #training_data_interval = ds.DataSetInterval(start_date='2011-12-01', end_date='2019-12-31')
        #tune_training_data_interval = ds.DataSetInterval(start_date='2014-08-01', end_date='2016-08-01')
        #tune_validation_data_interval = ds.DataSetInterval(start_date='2016-11-01', end_date='2016-12-31')
        data_set_definition = ds.DataSetDefinitionFactory.create_data_set_definition()
        return ExperimentModel(data_set_definition.out_seq)
