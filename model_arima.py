from pandas import read_csv
from sklearn.metrics import mean_squared_error
from pandas import Series
from pandas import DatetimeIndex
from data_set import TimeSeriesDataSet
from matplotlib import pyplot
import data_set as ap_data_set
from data_set import DataSetDefinition
from data_set import DataSetDefinitionFactory
from data_set import DataSetFactory
from statsmodels.tsa.arima_model import ARIMA
from data_set import DATA_NUM_STEPS
import time
import pandas as pd
from pandas import DataFrame
from experiment import Experiment
import experiment_results as results


class ModelArima:

    def __init__(self, experiment: Experiment):
        self.experiment = experiment
        self.data_set_definition = DataSetDefinitionFactory.create_definition(experiment.experiment_model)
        self.forecast_step = experiment.experiment_model.forecast
        self.predictions = list()
        self.real_values = list()
        self.results = results.Results()

    @staticmethod
    def uf_model_name():
        return 'ARIMA'

    def run(self):
        tic = time.perf_counter()
        self.results.reset()
        data_set = self.experiment.data_set
        test_data_set = self.experiment.test_data_set

        ns_out_seq = self.experiment.experiment_model.ns_out_seq
        out_seq = self.experiment.experiment_model.out_seq

        # train history
        train = data_set.get(ns_out_seq)
        train_values = train.values
        history = [x for x in train_values]

        #
        test_shifted_data = test_data_set.get(out_seq)
        shifted_data_values = test_shifted_data.values
        test = test_data_set.get(ns_out_seq)
        test_values = test.values

        for t in range(len(test)):
            model = ARIMA(history, order=(self.forecast_step, 1, 1))
            model_fit = model.fit(disp=0)

            fc, se, conf = model_fit.forecast(steps=self.forecast_step)
            yhat = fc[self.forecast_step-1]

            obs = shifted_data_values[t]
            next_value = test_values[t]
            history.append(next_value)
            print('predicted=%f, expected=%f' % (yhat, obs))

            self.results.predictions.append(yhat)
            self.results.real_values.append(obs)

        toc = time.perf_counter()
        exec_time = toc - tic
        print(exec_time)

        print('mse=%f, rmse=%f' % (mean_squared_error(test, self.results.predictions), mean_squared_error(test, self.results.predictions, squared=False)))
        self.results.metrics()

        self.results.y = Series(self.results.real_values, index=test.index)
        self.results.y_hat = Series(self.results.predictions, index=test.index)

        self.results.save(ModelArima.uf_model_name(), self.experiment.experiment_model)
        self.results.plot(results.ResultsPlot.ARIMA_FORECAST, self.experiment.experiment_model.ns_out_seq)
        return self.results

    @staticmethod
    def train():
        data_set_definition = DataSetDefinition(in_sequences=[ap_data_set.KARPOS_PM10],
                                                out_seq=ap_data_set.KARPOS_PM10_3,
                                                num_steps=DATA_NUM_STEPS)
        data_set = DataSetFactory.create('2019-12-01', '2019-12-31', data_set_definition)
        test_data_set = DataSetFactory.create_test_data_set(data_set_definition=data_set_definition)

        # test
        predictions = list()
        real_values = list()

        train = data_set.get(ap_data_set.KARPOS_PM10)
        train_values = train.values
        test_s3 = test_data_set.get(ap_data_set.KARPOS_PM10_3)
        test_s3_values = test_s3.values
        test = test_data_set.get(ap_data_set.KARPOS_PM10)
        test_values = test.values
        history = [x for x in train_values]

        for t in range(len(test)):
            tic = time.perf_counter()
            model = ARIMA(history, order=(DATA_NUM_STEPS, 1, 1))
            model_fit = model.fit(disp=0)
            toc = time.perf_counter()
            exec_time = toc - tic
            print(exec_time)

            fc, se, conf = model_fit.forecast(steps=4)
            yhat = fc[3]

            obs = test_s3_values[t]
            next_value = test_values[t]
            history.append(next_value)
            print('predicted=%f, expected=%f' % (yhat, obs))

            predictions.append(yhat)
            real_values.append(obs)

        error = mean_squared_error(test, predictions)

        y = Series(real_values, index=test.index)
        y_hat = Series(predictions, index=test.index)
        y.to_csv('arima_y.csv')
        y_hat.to_csv('arima_yhat.csv')

        # plot
        pyplot.plot(y, color='blue', label='Values')
        pyplot.plot(y_hat, color='red', label='Forecast')
        pyplot.legend(loc='upper left', fontsize=8)
        pyplot.show()

    def print_metrics(self, data_set: TimeSeriesDataSet, start: str, end: str):
        print(self.test_data_metrics(data_set=data_set, start=start, end=end))

    def test_data_metrics(self, data_set: TimeSeriesDataSet, start: str, end: str):
        df = self.get_df(data_set=data_set, start=start, end=end)
        return {
            'mse': mean_squared_error(df['y'], df['y_hat']),
            'rmse': mean_squared_error(df['y'], df['y_hat'], squared=False)
        }

    def get_df(self, data_set: TimeSeriesDataSet, start: str, end: str):
        y_hat = ModelArima.load_arima_predictions()
        y = data_set.out_seq

        end_ts = pd.to_datetime(end)
        end_ts = end_ts - pd.Timedelta(DATA_NUM_STEPS, 'h')
        start_ts = pd.to_datetime(start)
        df_data = DataFrame({'y': y, 'y_hat': y_hat}, index=y.index)
        df = df_data.loc[start_ts:end_ts]
        return df

    @staticmethod
    def load_arima_predictions() -> Series:
        arima_y_hat_file = "evaluation/old_experiments/arima_yhat.csv"
        arima_df = read_csv(arima_y_hat_file, index_col=0)
        arima = arima_df['pm10']
        arima.index = DatetimeIndex(arima.index.to_list())
        return arima
