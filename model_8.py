from kerastuner import HyperParameters
from tensorflow import keras
from tensorflow.keras.layers import LSTM
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import Input
from tensorflow.keras.layers import SimpleRNN
from tensorflow.keras.layers import Dropout
from hyper_models import AirQualityHyperModel
from lstm_models import ModelParams


class Model8(AirQualityHyperModel):

    def __init__(self, model_params: ModelParams, model_path_prefix='Case_19_', load_model=True):
        super().__init__(model_params=model_params, model_path_prefix=model_path_prefix, load_model=load_model)

    def build(self, hp: HyperParameters):
        model = keras.Sequential()
        units = hp.Int('initial_unit', min_value=2, max_value=24, step=2)
        rnn_units = hp.Int('rnn_unit', min_value=1, max_value=128, step=4)
        second_lstm_layer_units = hp.Int('second_lstm_layer_unit', min_value=2, max_value=124, step=4)

        # input_shape = (None, n_steps, n_features)
        model_input_shape = (self.model_params.n_steps, self.model_params.n_features)
        model.add(Input(shape=model_input_shape))
        print(model.output)
        # The output of SimpleRNN will be a 2D tensor of shape (batch_size, 128)
        model.add(SimpleRNN(rnn_units, return_sequences=True, activation=self.model_params.activation))
        print(model.output)
        model.add(LSTM(units=units, activation=self.model_params.activation, return_sequences=True))
        print(model.output)
        model.add(Dropout(rate=hp.Choice('dropout_rate', [0.2, 0.1])))
        print(model.output)
        model.add(LSTM(units=second_lstm_layer_units, activation=self.model_params.activation))
        print(model.output)
        model.add(Dense(1))
        print(model.output)

        model.compile(
            optimizer=keras.optimizers.Adam(hp.Choice('learning_rate', [0.01, 0.1])),
            loss=self.model_params.loss,
            metrics=self.model_params.metrics)
        return model
