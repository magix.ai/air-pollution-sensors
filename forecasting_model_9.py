from kerastuner import HyperParameters
from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras.layers import SimpleRNN
from tensorflow.keras.layers import Input
from hyper_models import AirPollutionForecastingModel


class ForecastingModel9(AirPollutionForecastingModel):

    def build(self, hp: HyperParameters):
        model = keras.Sequential()
        rnn_units = hp.Int('rnn_unit', min_value=1, max_value=128, step=4)
        model_input_shape = (self.model_params.n_steps, self.model_params.n_features)
        model.add(Input(shape=model_input_shape))
        print(model.output)
        # The output of SimpleRNN will be a 2D tensor of shape (batch_size, rnn_units)
        model.add(SimpleRNN(rnn_units, activation=self.model_params.activation, return_sequences=True))
        print(model.output)
        second_rnn_layer_units = hp.Int('second_enn_layer_unit', min_value=2, max_value=124, step=4)
        model.add(layers.SimpleRNN(units=second_rnn_layer_units, activation=self.model_params.activation))
        model.add(layers.Dense(1))

        model.compile(
            optimizer=keras.optimizers.Adam(hp.Choice('learning_rate', [0.01, 0.1])),
            loss=self.model_params.loss,
            metrics=self.model_params.metrics)
        return model
