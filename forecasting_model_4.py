from kerastuner import HyperParameters
from tensorflow import keras
from tensorflow.keras import layers
from hyper_models import AirPollutionForecastingModel


class ForecastingModel4(AirPollutionForecastingModel):

    def build(self, hp: HyperParameters):
        n_features = self.model_params.n_features
        model_input_shape = (self.model_params.n_steps, n_features)

        model = keras.Sequential()
        units = hp.Int('initial_unit', min_value=2, max_value=24, step=2)
        second_lstm_layer_units = hp.Int('second_lstm_layer_unit', min_value=2, max_value=124, step=4)
        model.add(layers.LSTM(units=units, activation=self.model_params.activation, input_shape=model_input_shape,
                              return_sequences=True))
        model.add(layers.Dropout(rate=hp.Choice('dropout_rate', [0.3, 0.2, 0.1])))
        model.add(layers.LSTM(units=second_lstm_layer_units, activation=self.model_params.activation))
        model.add(layers.Dense(1))

        model.compile(
            optimizer=keras.optimizers.Adam(hp.Choice('learning_rate', [0.01, 0.1])),
            loss=self.model_params.loss,
            metrics=self.model_params.metrics)
        return model
