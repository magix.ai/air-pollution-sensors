from data_set import KARPOS_PM10_24
from experiment_model import ArimaExperimentModel24
from experiment import Experiment
import model_arima as arima_forecasting_model

if __name__ == "__main__":
    EVALUATION_START_DATE = '2020-01-01'
    EVALUATION_END_DATE = '2020-02-01'
    experiment_model = ArimaExperimentModel24(out_seq=KARPOS_PM10_24)
    experiment = Experiment(experiment_model=experiment_model)
    model = arima_forecasting_model.ModelArima(experiment)
    model.run()
