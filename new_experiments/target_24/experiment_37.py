from data_set import KARPOS_PM10_24
from forecasting_model_convolutional import ForecastingModelConvolutional
from experiment_model import ExperimentModel24
from experiment import Experiment

if __name__ == "__main__":
    EVALUATION_START_DATE = '2020-01-01'
    EVALUATION_END_DATE = '2020-02-01'
    experiment_model = ExperimentModel24(out_seq=KARPOS_PM10_24)
    experiment_37 = Experiment(experiment_model=experiment_model)
    model = ForecastingModelConvolutional(experiment=experiment_37, load_model=False, model_description='Convolutional_Stojov')
    model.run(start_date=EVALUATION_START_DATE, end_date=EVALUATION_END_DATE)
