from data_set import KARPOS_PM10_24
from model_1 import Model1
from experiment_model import ExperimentModel24
from experiment import Experiment

if __name__ == "__main__":
    EVALUATION_START_DATE = '2020-01-01'
    EVALUATION_END_DATE = '2020-02-01'
    experiment_model = ExperimentModel24(out_seq=KARPOS_PM10_24)
    experiment_21 = Experiment(experiment_model=experiment_model)
    model = Model1(experiment=experiment_21, load_model=False, model_description='Experiment_21')
    model.run(start_date=EVALUATION_START_DATE, end_date=EVALUATION_END_DATE)