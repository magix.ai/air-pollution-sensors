import data_set as data_set
from experiment_model import CompleteDataSetModel
from experiment import Experiment

if __name__ == "__main__":
    experiment_model = CompleteDataSetModel(out_seq=data_set.REKTORAT_PM10_6)
    experiment = Experiment(experiment_model=experiment_model)
    data = experiment.data_set
    data.plot(data_set.MILADINOVCI_PM25)

    data.plot(data_set.CENTAR_PM25)

    data.plot(data_set.REKTORAT_PM25)

    data.plot(data_set.KARPOS_PM25)
