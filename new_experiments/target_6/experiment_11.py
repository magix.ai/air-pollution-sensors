from data_set import REKTORAT_PM10_6
from model_1 import Model1
from experiment_model import ExperimentModel
from experiment import Experiment

if __name__ == "__main__":
    EVALUATION_START_DATE = '2020-01-01'
    EVALUATION_END_DATE = '2020-02-01'
    experiment_model = ExperimentModel(out_seq=REKTORAT_PM10_6)
    experiment_11 = Experiment(experiment_model=experiment_model)
    model = Model1(experiment=experiment_11, model_description='Lstm+Dense', load_model=False)
    model.run(start_date=EVALUATION_START_DATE, end_date=EVALUATION_END_DATE)
