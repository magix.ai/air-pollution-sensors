from data_set import KARPOS_PM10_6
from experiment_model import ArimaExperimentModel6Year
from experiment import Experiment
from model_arima import ModelArima

if __name__ == "__main__":
    EVALUATION_START_DATE = '2019-02-01'
    EVALUATION_END_DATE = '2020-02-01'
    experiment_model = ArimaExperimentModel6Year(out_seq=KARPOS_PM10_6)
    experiment = Experiment(experiment_model=experiment_model)
    model = ModelArima(experiment)
    model.run()
