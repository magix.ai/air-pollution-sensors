from data_set import REKTORAT_PM10_6
from forecasting_model_4 import ForecastingModel4
from experiment_model import ExperimentModel
from experiment import Experiment

if __name__ == "__main__":
    EVALUATION_START_DATE = '2020-01-01'
    EVALUATION_END_DATE = '2020-02-01'
    experiment_model = ExperimentModel(out_seq=REKTORAT_PM10_6)
    model = ForecastingModel4(experiment=Experiment(experiment_model=experiment_model), load_model=False,
                              model_description='Lstm+Dropout_Experiment12')
    model.run(start_date=EVALUATION_START_DATE, end_date=EVALUATION_END_DATE)
