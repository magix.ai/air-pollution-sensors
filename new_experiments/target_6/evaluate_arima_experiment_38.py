from data_set import REKTORAT_PM10_6
from experiment_model import ArimaExperimentModel6
from experiment import Experiment
from model_arima import ModelArima

if __name__ == "__main__":
    EVALUATION_START_DATE = '2020-01-01'
    EVALUATION_END_DATE = '2020-02-01'
    experiment_model = ArimaExperimentModel6(out_seq=REKTORAT_PM10_6)
    experiment = Experiment(experiment_model=experiment_model)
    model = ModelArima(experiment)
    model.run()
