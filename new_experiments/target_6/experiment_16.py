from data_set import REKTORAT_PM10_6
from forecasting_model_9 import ForecastingModel9
from experiment_model import ExperimentModel
from experiment import Experiment

if __name__ == "__main__":
    EVALUATION_START_DATE = '2020-01-01'
    EVALUATION_END_DATE = '2020-02-01'
    experiment_model = ExperimentModel(out_seq=REKTORAT_PM10_6)
    experiment16 = Experiment(experiment_model=experiment_model)
    model = ForecastingModel9(experiment=experiment16, model_description='SimpleRNN+SimpleRNN+Dense', load_model=False)
    model.run(start_date=EVALUATION_START_DATE, end_date=EVALUATION_END_DATE)
