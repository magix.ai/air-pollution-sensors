from data_set import KARPOS_PM10_12
from model_1 import Model1
from experiment_model import No2DataSetModel12
from experiment import Experiment

if __name__ == "__main__":
    EVALUATION_START_DATE = '2020-01-01'
    EVALUATION_END_DATE = '2020-02-01'
    experiment_model = No2DataSetModel12(out_seq=KARPOS_PM10_12)
    experiment_32 = Experiment(experiment_model=experiment_model)
    model = Model1(experiment=experiment_32, model_description='Model1No2', load_model=False)
    model.run(start_date=EVALUATION_START_DATE, end_date=EVALUATION_END_DATE)
