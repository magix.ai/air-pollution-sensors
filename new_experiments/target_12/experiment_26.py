from data_set import KARPOS_PM10_12
from forecasting_model_convolutional_optimized import ForecastingModelConvolutionalOptimized
from experiment_model import ExperimentModel12
from experiment import Experiment

if __name__ == "__main__":
    EVALUATION_START_DATE = '2020-01-01'
    EVALUATION_END_DATE = '2020-02-01'
    experiment_model = ExperimentModel12(out_seq=KARPOS_PM10_12)
    model = ForecastingModelConvolutionalOptimized(experiment=Experiment(experiment_model=experiment_model))
    # model.metrics_plot = False
    model.run(start_date=EVALUATION_START_DATE, end_date=EVALUATION_END_DATE)
