from data_set import KARPOS_PM10_12
from lstm_dense import LstmDense
from experiment_model import PM25DataSetModel12
from experiment import Experiment

if __name__ == "__main__":
    EVALUATION_START_DATE = '2020-01-01'
    EVALUATION_END_DATE = '2020-02-01'
    experiment_model = PM25DataSetModel12(out_seq=KARPOS_PM10_12)
    model = LstmDense(experiment=Experiment(experiment_model=experiment_model))
    model.run(start_date=EVALUATION_START_DATE, end_date=EVALUATION_END_DATE)
