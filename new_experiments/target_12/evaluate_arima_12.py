from data_set import KARPOS_PM10_12
from experiment_model import ArimaExperimentModel12
from experiment import Experiment
from model_arima import ModelArima

if __name__ == "__main__":
    EVALUATION_START_DATE = '2020-01-01'
    EVALUATION_END_DATE = '2020-02-01'
    experiment_model = ArimaExperimentModel12(out_seq=KARPOS_PM10_12)
    experiment = Experiment(experiment_model=experiment_model)
    model = ModelArima(experiment)
    model.run()
