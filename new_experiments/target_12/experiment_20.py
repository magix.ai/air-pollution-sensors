from data_set import KARPOS_PM10_12
from forecasting_model_7 import ForecastingModel7
from experiment_model import ExtendedDataSetModel12
from experiment import Experiment

if __name__ == "__main__":
    EVALUATION_START_DATE = '2020-01-01'
    EVALUATION_END_DATE = '2020-02-01'
    experiment_model = ExtendedDataSetModel12(out_seq=KARPOS_PM10_12)
    model = ForecastingModel7(experiment=Experiment(experiment_model=experiment_model))
    model.run(start_date=EVALUATION_START_DATE, end_date=EVALUATION_END_DATE)
