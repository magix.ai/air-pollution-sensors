from data_set import KARPOS_PM10_12
from rnn_lstm_dropout_rnn import RnnLstmDropoutRnn
from experiment_model import ExperimentModel12
from experiment import Experiment

if __name__ == "__main__":
    EVALUATION_START_DATE = '2020-01-01'
    EVALUATION_END_DATE = '2020-02-01'
    experiment_model = ExperimentModel12(out_seq=KARPOS_PM10_12)
    experiment_50 = Experiment(experiment_model=experiment_model)
    model = RnnLstmDropoutRnn(experiment=experiment_50, load_model=False, model_description="Experiment_50")
    model.run(start_date=EVALUATION_START_DATE, end_date=EVALUATION_END_DATE)
