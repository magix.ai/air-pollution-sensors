from numpy import array
from numpy import hstack
import pandas as pd
import preprocessing
import matplotlib.pyplot as plt
from experiment_model import ExperimentModel

DATA_NUM_STEPS = 12
DATA_24_NUM_STEPS = 24
TARGET_SENSOR = 'Karpos_value_pm10__+3'

MILADINOVCI_PM10 = 'Miladinovci_value_pm10'
MILADINOVCI_PM10_3 = 'Miladinovci_value_pm10__+3'
LISICE_PM10 = 'Lisice_value_pm10'
LISICE_PM10_3 = 'Lisice_value_pm10__+3'
LISICE_PM10_6 = 'Lisice_value_pm10__+6'
LISICE_PM10_9 = 'Lisice_value_pm10__+9'
REKTORAT_PM10 = 'Rektorat_value_pm10'
REKTORAT_PM10_3 = 'Rektorat_value_pm10__+3'
REKTORAT_PM10_6 = 'Rektorat_value_pm10__+6'
REKTORAT_PM10_9 = 'Rektorat_value_pm10__+9'
GAZI_BABA_PM10 = 'Gazi Baba_value_pm10'
CENTAR_PM10 = 'Centar_value_pm10'
KARPOS_PM10 = 'Karpos_value_pm10'
KARPOS_PM10_3 = 'Karpos_value_pm10__+3'
KARPOS_PM10_6 = 'Karpos_value_pm10__+6'
KARPOS_PM10_9 = 'Karpos_value_pm10__+9'
KARPOS_PM10_12 = 'Karpos_value_pm10__+12'
KARPOS_PM10_24 = 'Karpos_value_pm10__+24'
KARPOS_PM25 = 'Karpos_value_pm25'
REKTORAT_PM25 = 'Rektorat_value_pm25'
MILADINOVCI_PM25 = 'Miladinovci_value_pm25'
CENTAR_PM25 = 'Centar_value_pm25'

CENTAR_O3 = 'Centar_value_o3'
KARPOS_O3 = 'Karpos_value_o3'
LISICE_O3 = 'Lisice_value_o3'
MILADINOVCI_O3 = 'Miladinovci_value_o3'
REKTORAT_O3 = 'Rektorat_value_o3'

LISICE_CO = 'Lisice_value_co'
CENTAR_CO = 'Centar_value_co'
KARPOS_CO = 'Karpos_value_co'

KARPOS_SO2 = 'Karpos_value_so2'
KARPOS_NO2 = 'Karpos_value_no2'

T_C = 'Skopje-petrovec_T_(C)'
T_DC = 'Skopje-petrovec_Td_(C)'
T_MAX = 'Skopje-petrovec_Tmax_(C)'
T_MIN = 'Skopje-petrovec_Tmin_(C)'
P_HPA = 'Skopje-petrovec_P sea_hPa'
P0_HPA = 'Skopje-petrovec_P0_hPa'
P_TND = 'Skopje-petrovec_P_Tnd'


class DataSetInterval:

    def __init__(self, start_date: str, end_date: str):
        self.start_date = start_date
        self.end_date = end_date


class DataSetDefinition:
    def __init__(self, in_sequences: list = None, out_seq: str = None, num_steps: int = DATA_NUM_STEPS):
        self.in_seq = in_sequences
        self.out_seq = out_seq
        self.n_features = len(in_sequences)
        self.num_steps = num_steps


class DataSetDefinitionFactory:

    @staticmethod
    def get_in_sequences():
        return [KARPOS_PM10, REKTORAT_PM10, MILADINOVCI_PM10, CENTAR_PM10, T_C, P0_HPA]

    @staticmethod
    def get_extended_in_sequences():
        return [KARPOS_PM10, REKTORAT_PM10, MILADINOVCI_PM10, CENTAR_PM10, T_C, P0_HPA, T_MIN, T_MAX]

    @staticmethod
    def get_all_in_sequences():
        return [KARPOS_PM10, REKTORAT_PM10, MILADINOVCI_PM10, CENTAR_PM10, T_C, P0_HPA, T_MIN, T_MAX, KARPOS_O3,
                MILADINOVCI_O3, LISICE_O3, REKTORAT_O3, CENTAR_O3, KARPOS_CO, LISICE_CO, CENTAR_CO, KARPOS_SO2,
                KARPOS_NO2, KARPOS_PM25, MILADINOVCI_PM25, CENTAR_PM25, REKTORAT_PM25]

    @staticmethod
    def create_definition(model: ExperimentModel, window_length=DATA_NUM_STEPS):
        data_set_definition = DataSetDefinition(in_sequences=model.in_seq, out_seq=model.out_seq,
                                                num_steps=window_length)
        return data_set_definition

    @staticmethod
    def create_data_set_definition():
        in_sequences = [KARPOS_PM10, REKTORAT_PM10, MILADINOVCI_PM10, CENTAR_PM10, T_C, P0_HPA]
        data_set_definition = DataSetDefinition(in_sequences, out_seq=KARPOS_PM10_3)
        return data_set_definition

    @staticmethod
    def create_data_set_4xpm10_definition():
        in_sequences = [KARPOS_PM10, REKTORAT_PM10, MILADINOVCI_PM10, CENTAR_PM10]
        data_set_definition = DataSetDefinition(in_sequences, out_seq=KARPOS_PM10_3)
        return data_set_definition

    @staticmethod
    def create_data_set_24_definition():
        in_sequences = [KARPOS_PM10, REKTORAT_PM10, MILADINOVCI_PM10, CENTAR_PM10, T_C, P0_HPA]
        data_set_definition = DataSetDefinition(in_sequences, out_seq=KARPOS_PM10_3, num_steps=DATA_24_NUM_STEPS)
        return data_set_definition

    @staticmethod
    def create_univariate_data_set_definition():
        in_sequences = [KARPOS_PM10]
        data_set_definition = DataSetDefinition(in_sequences, out_seq=KARPOS_PM10_3)
        return data_set_definition

    @staticmethod
    def create_two_sensors_data_set_definition():
        in_sequences = [KARPOS_PM10, REKTORAT_PM10]
        data_set_definition = DataSetDefinition(in_sequences, out_seq=KARPOS_PM10_3)
        return data_set_definition


class TimeSeriesDataSet(object):
    def __init__(self, in_sequences: list, in_sequences_dict: dict, out_seq: pd.Series, num_steps: int,
                 config: DataSetDefinition):
        self.seq_dict = in_sequences_dict
        self.in_seq = in_sequences
        self.out_seq = out_seq
        self.num_steps = num_steps
        self.data_set = self.prepare()
        self.X, self.y = self.as_model_data()
        self.data = (self.X, self.y)
        self.config = config

        self.start_date = self.out_seq.index.min()
        self.end_date = self.out_seq.index.max()

    def prepare(self):
        init_seq = self.in_seq.pop()
        data_set = init_seq.to_numpy().reshape((len(init_seq), 1))

        for in_seq in self.in_seq:
            in_seq = in_seq.to_numpy().reshape((len(in_seq), 1))
            data_set = hstack((data_set, in_seq))
        self.in_seq.append(init_seq)

        out_seq = self.out_seq.to_numpy().reshape((len(self.out_seq), 1))
        return hstack((data_set, out_seq))

    def get(self, column: str) -> pd.Series:
        return self.seq_dict[column]

    # split a multivariate sequence into samples
    def as_model_data(self):
        sequences = self.data_set
        n_steps = self.num_steps
        X, y = list(), list()
        for i in range(len(sequences)):
            # find the end of this pattern
            end_ix = i + n_steps
            # check if we are beyond the dataset
            if end_ix > len(sequences):
                break
            # gather input and output parts of the pattern
            seq_x, seq_y = sequences[i:end_ix, :-1], sequences[end_ix - 1, -1]
            X.append(seq_x)
            y.append(seq_y)
        return array(X), array(y)

    def plot(self, sequence_name, format="-", start=0, end=None):
        plt.figure(figsize=(10, 6))
        plt.grid(True)
        in_seq = self.get(column=sequence_name)

        plt.xlabel("Time")
        plt.ylabel(in_seq.name)
        plt.plot(in_seq.index.to_numpy(), in_seq.values)
        plt.show()

    def plot_prediction(self, sequence_name, format="-", start=0, end=None):
        plt.figure(figsize=(10, 6))
        # plt.grid(True)
        in_seq = self.get(column=sequence_name)

        plt.xlabel("Time")
        plt.ylabel(in_seq.name)
        plt.plot(in_seq.index.to_numpy(), in_seq.values)
        plt.show()

    def plot_sequence(self, sequence_name, format="-", start=0, end=None):
        plt.figure(figsize=(10, 6))
        in_seq = self.get(column=sequence_name)
        plt.xlabel("Time")
        plt.ylabel(in_seq.name)
        plt.plot(in_seq.index.to_numpy(), in_seq.values)
        plt.show()

    def plot_all(self, format="-", start=0, end=None):
        fig, axs = plt.subplots(len(self.in_seq))
        fig.set_size_inches(10, 35)
        i = 0
        for in_seq in self.in_seq:
            plot_y = in_seq.name
            axs[i].set(xlabel='Time', ylabel=plot_y.replace('_value', '').replace('Skopje-', ''))
            axs[i].plot(in_seq.index.to_numpy(), in_seq.values)
            i += 1
        plt.show()


class DataSetFactory:
    TRAIN_DATA = 'TRAIN'
    TUNE_TRAIN = 'TUNE_TRAIN'
    TUNE_VALIDATION = 'TUNE_VALIDATION'
    TEST_DATA = 'TEST'

    @staticmethod
    def get_data(model: ExperimentModel, data_type: str) -> TimeSeriesDataSet:
        data_set_definition = DataSetDefinitionFactory.create_definition(model)

        if data_type == DataSetFactory.TRAIN_DATA:
            return DataSetFactory.create(model.training_data_interval.start_date, model.training_data_interval.end_date,
                                         data_set_definition)
        elif data_type == DataSetFactory.TEST_DATA:
            return DataSetFactory.create(model.test_data_interval.start_date, model.test_data_interval.end_date,
                                         data_set_definition)
        elif data_type == DataSetFactory.TUNE_TRAIN:
            return DataSetFactory.create(model.tune_training_data_interval.start_date,
                                         model.tune_training_data_interval.end_date,
                                         data_set_definition)
        elif data_type == DataSetFactory.TUNE_VALIDATION:
            return DataSetFactory.create(model.tune_validation_data_interval.start_date,
                                         model.tune_validation_data_interval.end_date,
                                         data_set_definition)
        else:
            raise Exception("Incorrect data set.")

    @staticmethod
    def create_small_train_data_set(data_set_definition: DataSetDefinition) -> TimeSeriesDataSet:
        return DataSetFactory.create('2019-12-01', '2019-12-31', data_set_definition)

    @staticmethod
    def create_test_data_set(data_set_definition: DataSetDefinition) -> TimeSeriesDataSet:
        return DataSetFactory.create('2020-01-01', '2020-02-01', data_set_definition)

    @staticmethod
    def create_extended_test_data_set(data_set_definition: DataSetDefinition) -> TimeSeriesDataSet:
        return DataSetFactory.create('2019-12-31', '2020-02-01', data_set_definition)

    @staticmethod
    def create_train_data_set(data_set_definition: DataSetDefinition) -> TimeSeriesDataSet:
        return DataSetFactory.create('2011-12-01', '2019-12-31', data_set_definition)

    @staticmethod
    def create_tune_train_data_set(data_set_definition: DataSetDefinition) -> TimeSeriesDataSet:
        tune_training_data = DataSetFactory.create('2014-08-01', '2016-08-01', data_set_definition)
        return tune_training_data

    @staticmethod
    def create_tune_validation_data_set(data_set_definition: DataSetDefinition) -> TimeSeriesDataSet:
        tune_validation_data = DataSetFactory.create('2016-11-01', '2016-12-31', data_set_definition)
        return tune_validation_data

    @staticmethod
    def create(start_date: str, end_date: str, definition: DataSetDefinition, normalize_data=True) -> TimeSeriesDataSet:
        num_steps = definition.num_steps
        training_df = preprocessing.factory_data_df(
            pd.to_datetime(start_date),
            pd.to_datetime(end_date)
        )

        selected_columns = definition.in_seq + []
        selected_columns.append(definition.out_seq)

        trimmed_df = preprocessing.trim_df(training_df, selected_columns)
        out_seq = trimmed_df[definition.out_seq]
        if normalize_data:
            out_seq = ((out_seq - out_seq.min()) / (out_seq.max() - out_seq.min()))

        series = []
        series_dict = {definition.out_seq: out_seq}

        for column in definition.in_seq:
            data_sequence = trimmed_df[column]
            if normalize_data:
                data_sequence = ((data_sequence - data_sequence.min()) / (data_sequence.max() - data_sequence.min()))

            series.append(data_sequence)
            series_dict[column] = data_sequence

        data_set = TimeSeriesDataSet(series, series_dict, out_seq, num_steps, config=definition)
        return data_set
