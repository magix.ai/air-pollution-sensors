from data_set import DataSetFactory
from experiment_model import ExperimentModel
from lstm_models import ModelParams


class Experiment:
    def __init__(self, experiment_model: ExperimentModel):
        self.experiment_model = experiment_model
        self.data_set = DataSetFactory.get_data(self.experiment_model, DataSetFactory.TRAIN_DATA)
        self.tune_training_data = DataSetFactory.get_data(self.experiment_model, DataSetFactory.TUNE_TRAIN)
        self.tune_validation_data = DataSetFactory.get_data(self.experiment_model, DataSetFactory.TUNE_VALIDATION)
        self.test_data_set = DataSetFactory.get_data(self.experiment_model, DataSetFactory.TEST_DATA)
        self.model_params = ModelParams(self.data_set, metrics=[])
        self.model_params.activation = experiment_model.activation
        self.model_params.max_trials = experiment_model.max_trials

    def init_data_sets(self):
        self.data_set = DataSetFactory.get_data(self.experiment_model, DataSetFactory.TRAIN_DATA)
        self.tune_training_data = DataSetFactory.get_data(self.experiment_model, DataSetFactory.TUNE_TRAIN)
        self.tune_validation_data = DataSetFactory.get_data(self.experiment_model, DataSetFactory.TUNE_VALIDATION)
        self.test_data_set = DataSetFactory.get_data(self.experiment_model, DataSetFactory.TEST_DATA)
        self.model_params = ModelParams(self.data_set, metrics=[])
