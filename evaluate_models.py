from model_evaluate import ModelEvaluate
from model_3 import Model3
from model_2 import Model2
from model_1 import Model1
from model_8 import Model8
from model_9 import Model9
from model_4 import Model4
from model_5 import Model5
from model_6 import Model6
from model_arima import ModelArima
from model_11 import Model11

if __name__ == "__main__":

    EVALUATION_START_DATE = '2020-01-01'
    EVALUATION_END_DATE = '2020-02-01'

    model = ModelEvaluate()
    aq_model_2 = Model2(model_params=model.model_params)
    aq_model_1 = Model1(model_params=model.model_params)
    aq_model_3 = Model3(model_params=model.model_params)
    aq_model_8 = Model8(model_params=model.model_params)
    aq_model_9 = Model9(model_params=model.model_params)
    aq_model_4 = Model4(model_params=model.model_params)
    aq_model_5 = Model5(model_params=model.model_params)
    aq_model_6 = Model6(model_params=model.model_params)
    aq_model_11 = Model11(model_params=model.model_params)
    arima = ModelArima()

    #model.evaluate_4xpm10(aq_model_11, start_date=EVALUATION_START_DATE, end_date=EVALUATION_END_DATE)

    #model.test_metrics(aq_model_3, start_date=EVALUATION_START_DATE, end_date=EVALUATION_END_DATE)
    #model.evaluate_with_24_steps_data(aq_model_9, start_date=EVALUATION_START_DATE, end_date=EVALUATION_END_DATE)
    #model.test_metrics(aq_model_9, start_date=EVALUATION_START_DATE, end_date=EVALUATION_END_DATE)

    #model.evaluate(aq_model_9, '2020-01-01', '2020-02-01')
    model.evaluate(aq_model_5, '2020-01-01', '2020-02-01')
    #model.evaluate_with_two_sensors(aq_model_2, start_date=EVALUATION_START_DATE, end_date=EVALUATION_END_DATE)
    #model.evaluate_univariate(aq_model_1, start_date=EVALUATION_START_DATE, end_date=EVALUATION_END_DATE)
    model.compare_to_arima(aq_model_5, '2020-01-01', '2020-02-01')
    # aq_model_1 = Model1(model_params=model.model_params)
    # model.evaluate_univariate(aq_model_1, start_date=EVALUATION_START_DATE, end_date=EVALUATION_END_DATE)
    #model.test_arima_metrics(arima=arima, start_date=EVALUATION_START_DATE, end_date=EVALUATION_END_DATE)

