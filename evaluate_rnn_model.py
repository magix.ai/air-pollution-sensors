from model_rnn import ModelRnn
from model_bidirectional_rnn import ModelBidirectionalRnn
from model_evaluate import ModelEvaluate
from model_conv import ModelConv
from model_gru_rnn import ModelGruRnn
from model_4 import Model4
from model_5 import Model5
from model_6 import Model6

if __name__ == "__main__":

    model = ModelEvaluate()
    #aq_model = ModelRnn(model_params=model.model_params, model_path_prefix = 'Case_10_', load_model=True)
    #aq_model = ModelBidirectionalRnn(model_params=model.model_params, model_path_prefix='Case_12_', load_model=True)
    #aq_model = ModelConv(model_params=model.model_params, model_path_prefix='Case_14_', load_model=True)

    aq_model_4 = Model4(model_params=model.model_params)
    #aq_model_5 = Model5(model_params=model.model_params)
    #aq_model_6 = Model6(model_params=model.model_params)
    #aq_model.tune(model.tune_training_data, model.tune_validation_data)
    #aq_model.summary()

    model.evaluate(aq_model_4, start_date='2020-01-01', end_date='2020-02-01')
    model.test_metrics(aq_model_4, start_date='2020-01-01', end_date='2020-02-01')
    #model.compare_to_arima(aq_model, start_date='2020-01-01', end_date='2020-01-04')
