from model_evaluate import ModelEvaluate
from model_3 import Model3

if __name__ == "__main__":

    EVALUATION_START_DATE = '2020-01-01'
    EVALUATION_END_DATE = '2020-02-01'

    model = ModelEvaluate()
    aq_model_3 = Model3(model_params=model.model_params)
    model.evaluate(aq_model_3, start_date=EVALUATION_START_DATE, end_date=EVALUATION_END_DATE)


