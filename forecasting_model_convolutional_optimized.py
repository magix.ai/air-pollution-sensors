from kerastuner import HyperParameters
from tensorflow import keras
from tensorflow.keras.layers import Conv2D
from tensorflow.keras.layers import MaxPooling2D
from tensorflow.keras.layers import Flatten
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import Input
from hyper_models import AirPollutionForecastingModel
from numpy import ndarray


class ForecastingModelConvolutionalOptimized(AirPollutionForecastingModel):

    def prepare_data(self):
        tune_data = self.experiment.tune_training_data
        tune_data.X = self.reshape_data(tune_data.X)
        tune_validation_data = self.experiment.tune_validation_data
        tune_validation_data.X = self.reshape_data(tune_validation_data.X)

        validation_data = tune_validation_data.data
        tune_validation_data.data = (self.reshape_data(validation_data[0]), validation_data[1])

        training_data = self.experiment.data_set
        training_data.X = self.reshape_data(training_data.X)

        test_data = self.experiment.test_data_set
        test_data.X = self.reshape_data(test_data.X)

    def reshape_data(self, data: ndarray) -> ndarray:
        bulk_size = data.shape[0]
        x_steps = data.shape[1]
        x_features = data.shape[2]

        reshaped_data = data.reshape(bulk_size, 1, x_steps, x_features)
        return reshaped_data

    def format_data_sample(self, x: ndarray) -> ndarray:
        bulk_size = 1
        x_channels = x.shape[0]
        x_steps = x.shape[1]
        x_features = x.shape[2]

        reshaped_x = x.reshape(bulk_size, x_channels, x_steps, x_features)
        return reshaped_x

    def build(self, hp: HyperParameters):
        model = keras.Sequential()

        first_layer_filters = 6
        second_layer_filters = 6
        third_layer_filters = hp.Choice('third_layer_filters', [4, 6])
        output_dense_units = 1
        model_input_shape = (1, self.model_params.n_steps, self.model_params.n_features)

        conv_kernel_width = hp.Int('conv_kernel_width', min_value=3, max_value=6, step=1)
        conv_kernel_height = hp.Int('conv_kernel_height', min_value=3, max_value=6, step=1)

        second_layer_conv_kernel_width = 3  # hp.Int('second_layer_conv_kernel_width', min_value=1,
        #    max_value=conv_kernel_width, step=1)
        second_layer_conv_kernel_height = 3  # hp.Int('second_layer_conv_kernel_height', min_value=1,
        #     max_value=conv_kernel_height, step=1)

        third_layer_conv_kernel_width = 3  # hp.Int('second_layer_conv_kernel_width', min_value=1,
        #    max_value=conv_kernel_width, step=1)
        third_layer_conv_kernel_height = 3  # hp.Int('second_layer_conv_kernel_height', min_value=1,
        #     max_value=conv_kernel_height, step=1)

        pool_size_1 = 2  # hp.Int('pool_size_1', min_value=1, max_value=2, step=1)
        pool_size_2 = 2  # hp.Int('pool_size_2', min_value=1, max_value=2, step=1)

        print(model)
        model.add(Input(shape=model_input_shape))

        # Conv 1st layer
        model.add(Conv2D(first_layer_filters,
                         kernel_size=(conv_kernel_width, conv_kernel_height),
                         strides=(1, 1),
                         padding='same',
                         data_format='channels_first',
                         activation='relu'))
        print(model.output)
        model.add(MaxPooling2D(pool_size=(pool_size_1, pool_size_2), strides=(1, 1)))
        print(model.output)

        # Conv 2nd layer
        model.add(Conv2D(second_layer_filters,
                         kernel_size=(second_layer_conv_kernel_width, second_layer_conv_kernel_height), strides=(1, 1),
                         activation='relu'))
        print(model.output)
        model.add(MaxPooling2D(pool_size=(1, 1)))
        print(model.output)

        # Conv 3rd layer
        model.add(Conv2D(third_layer_filters,
                         kernel_size=(third_layer_conv_kernel_width, third_layer_conv_kernel_height), strides=(1, 1),
                         activation='relu'))
        print(model.output)
        model.add(MaxPooling2D(pool_size=(1, 1)))
        print(model.output)

        model.add(Flatten())
        print(model.output)

        model.add(Dense(output_dense_units, activation='relu'))
        print(model.output)

        # model.compile(loss=keras.losses.categorical_crossentropy,
        #              optimizer=keras.optimizers.Adam(),
        #              metrics=['accuracy'])
        # End conv

        model.compile(
            optimizer=keras.optimizers.Adam(hp.Choice('learning_rate', [0.01, 0.1])),
            loss=self.model_params.loss,
            metrics=self.model_params.metrics)
        return model
