from lstm_models import ModelParams
from model_1 import Model1


class Model2(Model1):

    def __init__(self, model_params: ModelParams, model_path_prefix='Case_2_', load_model=True):
        super().__init__(model_params=model_params, model_path_prefix=model_path_prefix, load_model=load_model)
