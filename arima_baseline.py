import data_set as ap_data_set
from data_set import DataSetDefinition
from data_set import DataSetFactory
from statsmodels.tsa.arima_model import ARIMA
from sklearn.metrics import mean_squared_error
from matplotlib import pyplot
from pandas import Series
import time
from data_set import DATA_NUM_STEPS

if __name__ == "__main__":

    data_set_definition = DataSetDefinition(in_sequences=[ap_data_set.KARPOS_PM10], out_seq=ap_data_set.KARPOS_PM10_3,
                                            num_steps=DATA_NUM_STEPS)
    data_set = DataSetFactory.create('2019-12-01', '2019-12-31', data_set_definition)
    test_data_set = DataSetFactory.create_test_data_set(data_set_definition=data_set_definition)

    # test
    predictions = list()
    real_values = list()

    train = data_set.get(ap_data_set.KARPOS_PM10)
    train_values = train.values
    test_s3 = test_data_set.get(ap_data_set.KARPOS_PM10_3)
    test_s3_values = test_s3.values
    test = test_data_set.get(ap_data_set.KARPOS_PM10)
    test_values = test.values
    history = [x for x in train_values]

    for t in range(len(test)):
        tic = time.perf_counter()
        model = ARIMA(history, order=(DATA_NUM_STEPS, 1, 1))
        model_fit = model.fit(disp=0)
        toc = time.perf_counter()
        exec_time = toc - tic
        print(exec_time)

        fc, se, conf = model_fit.forecast(steps=4)
        yhat = fc[3]

        obs = test_s3_values[t]
        next_value = test_values[t]
        history.append(next_value)
        print('predicted=%f, expected=%f' % (yhat, obs))

        predictions.append(yhat)
        real_values.append(obs)

    error = mean_squared_error(test, predictions)

    y = Series(real_values, index=test.index)
    y_hat = Series(predictions, index=test.index)
    y.to_csv('arima_y.csv')
    y_hat.to_csv('arima_yhat.csv')

    # plot
    pyplot.plot(y, color='blue', label='Values')
    pyplot.plot(y_hat, color='red', label='Forecast')
    pyplot.legend(loc='upper left', fontsize=8)
    pyplot.show()
