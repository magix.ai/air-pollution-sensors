import lstm_models as ap_models
from lstm_models import ModelParams
from data_set import DataSetFactory
from data_set import DataSetDefinitionFactory
from hyper_models import AirQualityHyperModel
from model_arima import ModelArima


class ModelEvaluate:

    def __init__(self):
        self.data_set_definition = DataSetDefinitionFactory.create_data_set_definition()
        self.data_set = DataSetFactory.create_train_data_set(self.data_set_definition)
        self.tune_training_data = DataSetFactory.create_tune_train_data_set(self.data_set_definition)
        self.tune_validation_data = DataSetFactory.create_tune_validation_data_set(self.data_set_definition)
        self.test_data_set = DataSetFactory.create_extended_test_data_set(self.data_set_definition)
        self.model_params = ModelParams(self.data_set, metrics=[])

    def init_data_sets(self):
        self.data_set = DataSetFactory.create_train_data_set(self.data_set_definition)
        self.tune_training_data = DataSetFactory.create_tune_train_data_set(self.data_set_definition)
        self.tune_validation_data = DataSetFactory.create_tune_validation_data_set(self.data_set_definition)
        self.test_data_set = DataSetFactory.create_extended_test_data_set(self.data_set_definition)

    def init_model_params(self, aq_model: AirQualityHyperModel) -> AirQualityHyperModel:
        self.model_params = ModelParams(self.data_set, metrics=[])
        aq_model.model_params = ModelParams(self.data_set, metrics=[])
        return aq_model

    def evaluate(self, aq_model: AirQualityHyperModel, start_date: str, end_date: str):
        self.data_set_definition = DataSetDefinitionFactory.create_data_set_definition()
        self.init_data_sets()
        model = self.init_model_params(aq_model=aq_model)

        model.evaluate(self.data_set, self.tune_training_data, self.tune_validation_data)
        model.compare_to_arima(self.test_data_set, start_date, end_date, read_data_from_file=True)
        model.test_data_metrics(self.test_data_set, start_date, end_date, read_data_from_file=True)
        self.print_metrics(model, start_date, end_date)

    def evaluate_4xpm10(self, aq_model: AirQualityHyperModel, start_date: str, end_date: str):
        self.data_set_definition = DataSetDefinitionFactory.create_data_set_4xpm10_definition()
        self.evaluate_model(aq_model, start_date, end_date)

    def evaluate_model(self, aq_model: AirQualityHyperModel, start_date: str, end_date: str):
        self.init_data_sets()
        model = self.init_model_params(aq_model=aq_model)
        model.evaluate(self.data_set, self.tune_training_data, self.tune_validation_data)
        model.compare_to_arima(self.test_data_set, start_date, end_date, read_data_from_file=True)
        model.test_data_metrics(self.test_data_set, start_date, end_date, read_data_from_file=True)
        self.print_metrics(model, start_date, end_date)

    def evaluate_with_two_sensors(self, aq_model: AirQualityHyperModel, start_date: str, end_date: str):
        self.data_set_definition = DataSetDefinitionFactory.create_two_sensors_data_set_definition()
        self.evaluate_model(aq_model, start_date, end_date)

    def evaluate_with_24_steps_data(self, aq_model: AirQualityHyperModel, start_date: str, end_date: str):
        self.data_set_definition = DataSetDefinitionFactory.create_data_set_24_definition()
        self.evaluate_model(aq_model, start_date, end_date)

    def evaluate_univariate(self, aq_model: AirQualityHyperModel, start_date: str, end_date: str):
        self.data_set_definition = DataSetDefinitionFactory.create_univariate_data_set_definition()
        self.evaluate_model(aq_model, start_date, end_date)

    def test_metrics(self, aq_model: AirQualityHyperModel, start_date: str, end_date: str):
        print(aq_model.test_data_metrics(self.test_data_set, start_date, end_date, read_data_from_file=True))

    def print_metrics(self, aq_model: AirQualityHyperModel, start_date: str, end_date: str):
        print(aq_model.test_data_metrics(self.test_data_set, start_date, end_date, read_data_from_file=True))

    def test_arima_metrics(self, arima: ModelArima, start_date: str, end_date: str):
        arima.print_metrics(self.test_data_set, start_date, end_date)

    def compare_to_arima(self, aq_model: AirQualityHyperModel, start_date: str, end_date: str):
        aq_model.compare_to_arima(self.test_data_set, start_date, end_date, read_data_from_file=True)