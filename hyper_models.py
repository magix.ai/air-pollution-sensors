from kerastuner.tuners import RandomSearch
from data_set import TimeSeriesDataSet
from kerastuner import HyperModel, HyperParameters
from tensorflow import keras
from tensorflow.keras.callbacks import History
from lstm_models import ModelParams
from abc import ABC, abstractmethod
import matplotlib.pyplot as plt
import os
import numpy as np
from tensorflow.keras.callbacks import TensorBoard
import time
from pandas import Series
from pandas import read_csv
from pandas import DatetimeIndex
from data_set import DATA_NUM_STEPS
from data_set import TARGET_SENSOR
import pandas as pd
from pandas import DataFrame
from sklearn.metrics import mean_squared_error
from experiment import Experiment
from model_arima import ModelArima as ArimaForecastingModel
import experiment_results as experiment_results
import sys


class AirQualityHyperModel(HyperModel, ABC):

    def __init__(self, model_params: ModelParams, model_path_prefix: str, load_model=False):
        super().__init__()
        self.model_params = model_params
        self.model = None
        self.model_path_prefix = model_path_prefix
        self.metrics_plot = True
        self.descriptive_name = self.__class__.__name__

        if not self.is_model_serialized():
            self.model_params.metrics.append('accuracy')

        if load_model:
            self.load_model()

        self.history = History()

    @abstractmethod
    def build(self, hp: HyperParameters):
        pass

    def evaluate(self, data_set: TimeSeriesDataSet, tune_training: TimeSeriesDataSet, tune_val: TimeSeriesDataSet):
        tic = time.perf_counter()
        self.tune(tune_training, tune_val)
        self.train(data_set)
        toc = time.perf_counter()
        self.summary()
        self.save_model()

        if self.metrics_plot:
            self.plot_metrics()

        exec_time = toc - tic
        print(exec_time)

    def tune(self, train_data_set: TimeSeriesDataSet, validation_data_set: TimeSeriesDataSet):
        tuner = RandomSearch(
            self,
            objective='val_loss',
            max_trials=self.model_params.max_trials,
            executions_per_trial=2,
            directory='tuner',
            project_name=self._model_tuner_path())
        tuner.search(
            train_data_set.X,
            train_data_set.y,
            epochs=self.model_params.tune_epochs,
            validation_data=validation_data_set.data)
        best = tuner.get_best_models(num_models=1)
        self.model = best.pop()

    def train(self, data_set: TimeSeriesDataSet):
        if self.model is None:
            raise BaseException

        tensorboard_callback = TensorBoard(log_dir="./logs")
        # fit model
        self.history = self.model.fit(
            x=data_set.X,
            y=data_set.y,
            epochs=self.model_params.epochs,
            verbose=self.model_params.verbose,
            validation_split=self.model_params.validation_split,
            workers=2,
            callbacks=[tensorboard_callback]
        )

    def plot_metrics(self):
        # Plot training & validation accuracy values
        fig, axs = plt.subplots(len(self.history.params['metrics']))
        fig.set_size_inches(10, 35)
        i = 0
        for k, v in self.history.history.items():
            plot_y = k
            axs[i].set(xlabel='Epochs', ylabel=plot_y)
            axs[i].plot(v)
            i += 1
        plt.show()

    def summary(self):
        self.model.summary()

    def compare_to_arima(self, data_set: TimeSeriesDataSet, start: str, end: str, read_data_from_file=False):
        result = self.get_df(data_set=data_set, start=start, end=end, read_data_from_file=read_data_from_file)
        # plot
        plt.plot(result['y'], color='blue', label='PM10 Values')
        plt.plot(result['arima'], color='orange', label='ARIMA')
        plt.plot(result['y_hat'], color='red', label='Forecast')
        plt.legend(loc='upper left', fontsize=22)
        plt.show()

    @staticmethod
    def load_arima_predictions() -> Series:
        arima_y_hat_file = "evaluation/old_experiments/arima_yhat.csv"
        arima_df = read_csv(arima_y_hat_file, index_col=0)
        arima = arima_df['pm10']
        arima.index = DatetimeIndex(arima.index.to_list())
        return arima

    def load_model_predictions(self, data_set: TimeSeriesDataSet, read_data_from_file=False) -> Series:
        y_hat_file = "evaluation/yhat_" + self._model_name() + ".csv"
        if read_data_from_file and os.path.isfile(y_hat_file):
            y_hat_df = read_csv(y_hat_file, index_col=0)
            y_hat = y_hat_df[TARGET_SENSOR]
            y_hat.index = DatetimeIndex(y_hat_df.index.to_list())
        else:
            y_hat = self.predictions(data_set)
        return y_hat

    def test_data_metrics(self, data_set: TimeSeriesDataSet, start: str, end: str, read_data_from_file=False):
        df = self.get_df(data_set=data_set, start=start, end=end, read_data_from_file=read_data_from_file)
        return {
            'mse': mean_squared_error(df['y'], df['y_hat']),
            'rmse': mean_squared_error(df['y'], df['y_hat'], squared=False)
        }

    def get_df(self, data_set: TimeSeriesDataSet, start: str, end: str, read_data_from_file=False):
        y_hat_file = "evaluation/yhat__" + self._model_name() + ".csv"
        y_file = "evaluation/y__" + self._model_name() + ".csv"

        arima = AirQualityHyperModel.load_arima_predictions()
        y_hat = self.load_model_predictions(data_set=data_set, read_data_from_file=read_data_from_file)
        y = data_set.out_seq

        if not os.path.isfile(y_hat_file):
            y.to_csv(y_file, header=[TARGET_SENSOR])
            y_hat.to_csv(y_hat_file, header=[TARGET_SENSOR])

        end_ts = pd.to_datetime(end)
        end_ts = end_ts - pd.Timedelta(DATA_NUM_STEPS, 'h')
        start_ts = y_hat.index.min()  # pd.to_datetime(start)
        timestamps_index = pd.date_range(start=start_ts, end=end_ts, freq='H')

        # measurement_timestamps_min = raw_data_df.index.min()
        # measurement_timestamps_max = raw_data_df.index.max()
        # timestamps_index = pd.date_range(start=measurement_timestamps_min, end=measurement_timestamps_max, freq='H')
        # df = pd.DataFrame(index=timestamps_index)

        df_data = DataFrame({'y': y, 'y_hat': y_hat, 'arima': arima}, index=y.index)
        df = df_data.loc[start_ts:end_ts]
        return df

    def predictions(self, data_set: TimeSeriesDataSet) -> Series:
        indexes_len = len(data_set.X)
        predictions = list()
        real_values = list()
        index_list = data_set.out_seq.index.to_list()
        index_list.reverse()
        predictions_index = list()
        sample_index = 0

        for data_index in range(indexes_len):
            prediction_index = index_list.pop()

            if data_index < DATA_NUM_STEPS:
                continue

            x = data_set.X[sample_index]
            y = data_set.y[sample_index]
            y_hat = self.predict(x)
            predictions.append(y_hat[0][0])
            real_values.append(y)
            predictions_index.append(prediction_index)
            sample_index += 1

        prediction_series = Series(predictions, index=DatetimeIndex(predictions_index))
        return prediction_series

    def predict(self, x_input):
        x_input = x_input.reshape((1, self.model_params.n_steps, self.model_params.n_features))
        y_hat = self.model.predict(x_input, verbose=0)
        return y_hat

    def plot_prediction(self, data_set: TimeSeriesDataSet, sample_index: int):
        x = data_set.X[sample_index]
        y = data_set.y[sample_index]
        y_hat = self.predict(x)
        x_transpose = x.T
        for in_seq in x_transpose:
            plt.plot(in_seq)

        plt.plot(y, 'rx')
        plt.plot(np.squeeze(y_hat), 'go')

        plt.title('Model Prediction')
        plt_legend = data_set.config.in_seq
        plt_legend.append(data_set.config.out_seq)
        plt_legend.append('y_hat')
        plt.legend(plt_legend, loc='upper left')
        plt.show()

    def save_model(self):
        model_path = self._model_storage_path()
        self.model.save(model_path)

    def is_model_serialized(self):
        model_path = self._model_storage_path()
        return os.path.isdir(model_path)

    def load_model(self):
        model_path = self._model_storage_path()
        if os.path.isdir(model_path):
            self.model = keras.models.load_model(model_path)

    def _model_storage_path(self) -> str:
        return os.path.join("model", self._model_name())

    def _model_tuner_path(self) -> str:
        return self._model_name()

    def _model_tensorboard_logs_path(self) -> str:
        return os.path.join("logs", self._model_name())

    def uf_model_name(self):
        if self.descriptive_name is not None:
            return self.descriptive_name
        return self.__class__.__name__

    def _model_name(self) -> str:
        model_n = self.uf_model_name()
        return str(self.model_path_prefix) + '__' + str(model_n)


class AirPollutionForecastingModel(AirQualityHyperModel, ABC):

    def __init__(self, experiment: Experiment, model_description=None, load_model=True):
        self.experiment = experiment
        path_prefix = experiment.experiment_model.out_seq + '__in_' + str(len(experiment.experiment_model.in_seq))
        self.results = experiment_results.Results()
        super().__init__(model_params=experiment.model_params, model_path_prefix=path_prefix, load_model=load_model)
        if model_description is not None:
            self.descriptive_name = model_description

    def init_model_params(self):
        self.model_params = ModelParams(self.experiment.data_set, metrics=[])
        self.model_params.activation = self.experiment.experiment_model.activation

    def run(self, start_date: str, end_date: str):
        self.init_data_sets()
        self.init_model_params()
        self.prepare_data()
        self.evaluate(self.experiment.data_set, self.experiment.tune_training_data,
                      self.experiment.tune_validation_data)
        self.compare_to_arima(self.experiment.test_data_set, start_date, end_date, read_data_from_file=True)
        self.test_data_metrics(self.experiment.test_data_set, start_date, end_date, read_data_from_file=True)
        self.print_metrics(start_date, end_date)

    def trial_run_file(self):
        file_path = os.path.join('results', self.uf_model_name())

    def init_data_sets(self):
        self.experiment.init_data_sets()

    def prepare_data(self):
        return

    def test_metrics(self, aq_model: AirQualityHyperModel, start_date: str, end_date: str):
        print(aq_model.test_data_metrics(self.experiment.test_data_set, start_date, end_date, read_data_from_file=True))

    def print_metrics(self, start_date: str, end_date: str):
        print(self.test_data_metrics(self.experiment.test_data_set, start_date, end_date, read_data_from_file=True))

    def test_arima_metrics(self, arima: ArimaForecastingModel, start_date: str, end_date: str):
        arima.print_metrics(self.experiment.test_data_set, start_date, end_date)

    def compare_results_to_arima(self, start_date: str, end_date: str):
        self.compare_to_arima(self.experiment.test_data_set, start_date, end_date, read_data_from_file=True)

    def get_df(self, data_set: TimeSeriesDataSet, start: str, end: str, read_data_from_file=False):
        arima_results = experiment_results.ResultsFactory.load_model_results(ArimaForecastingModel.uf_model_name(),
                                                                  self.experiment.experiment_model)
        y_hat = self.load_model_predictions(data_set=data_set, read_data_from_file=read_data_from_file)
        y = data_set.out_seq

        y_hat_file = experiment_results.Results.get_yhat_file_for_experiment(self.uf_model_name(),
                                                                  self.experiment.experiment_model)
        model_results = experiment_results.Results()
        model_results.y_hat = y_hat
        model_results.y = y

        if not os.path.isfile(y_hat_file):
            model_results.save(self.uf_model_name(), self.experiment.experiment_model)

        end_ts = pd.to_datetime(end)
        end_ts = end_ts - pd.Timedelta(self.experiment.experiment_model.forecast, 'h')
        start_ts = pd.to_datetime(start)  # model_results.y_hat.index.min()
        model_results.trim(start_ts, end_ts)
        df_data = DataFrame({'y': model_results.y, 'y_hat': model_results.y_hat, 'arima': arima_results.y_hat},
                            index=model_results.y.index)
        df = df_data.loc[start_ts:end_ts]
        return df

    def load_model_predictions(self, data_set: TimeSeriesDataSet, read_data_from_file=False) -> Series:
        y_hat_file = experiment_results.Results.get_yhat_file_for_experiment(
            self.uf_model_name(),
            self.experiment.experiment_model
        )
        if read_data_from_file and os.path.isfile(y_hat_file):
            y_hat_df = read_csv(y_hat_file, index_col=0)
            y_hat = y_hat_df[self.experiment.experiment_model.out_seq]
            y_hat.index = DatetimeIndex(y_hat_df.index.to_list())
        else:
            y_hat = self.predictions(data_set)
        return y_hat

    def predictions(self, data_set: TimeSeriesDataSet) -> Series:
        self.results = experiment_results.Results()
        self.results.reset()
        indexes_len = len(data_set.X)
        predictions = list()
        real_values = list()
        index_list = data_set.out_seq.index.to_list()
        index_list.reverse()
        predictions_index = list()
        sample_index = 0
        experiment_model = self.experiment.experiment_model

        for data_index in range(indexes_len):
            prediction_index = index_list.pop()

            if data_index < experiment_model.forecast:
                continue

            x = data_set.X[sample_index]
            y = data_set.y[sample_index]

            forecast = self.predict(x)
            y_hat = forecast[0][0]
            predictions.append(y_hat)
            real_values.append(y)
            predictions_index.append(prediction_index)
            sample_index += 1

            self.results.predictions.append(y_hat)
            self.results.real_values.append(y)

            print('Index=%s predicted=%f, expected=%f' % (prediction_index, y_hat, y))

        prediction_series = Series(predictions, index=DatetimeIndex(predictions_index))
        return prediction_series

    def format_data_sample(self, x: np.ndarray) -> np.ndarray:
        x_input = x.reshape((1, self.model_params.n_steps, self.model_params.n_features))
        return x_input

    def predict(self, x_input):
        x_formatted = self.format_data_sample(x_input)
        y_hat = self.model.predict(x_formatted, verbose=0)
        return y_hat
