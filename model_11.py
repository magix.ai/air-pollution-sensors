from lstm_models import ModelParams
from model_3 import Model3


class Model11(Model3):

    def __init__(self, model_params: ModelParams, model_path_prefix='Case_21_', load_model=True):
        super().__init__(model_params=model_params, model_path_prefix=model_path_prefix, load_model=load_model)