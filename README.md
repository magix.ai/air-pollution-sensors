# Air Pollution Skopje

Air Pollution Skopje is a project for testing and evaluation of LSTM based neural network architectures in forecasting air quality parameters.

## Results file naming template

yhat__{target}__ {model-name}__ {target forecast +h}__ {start date}__ { end date}__ {number of inputs}

## Preprocessing

Files preprocessing.py and data_set.py contain functions and classes for processing raw files from air pollution sensors and merge them with files containing meteorological data.
The merged processed files are then stored in 



predicted=0.455602, expected=0.495616

